const gulpBase = require('./gulpfile.base.js');
console.log(gulpBase);
gulpBase({
  server: {
    proxy: {
      target: 'http://127.0.0.1:8000/app'
    },
  },
  scripts: {
    files: ['./static/app/js/*.js']
  },
  sass: {
    files: [
      './app/bachillerato/core/**/*.scss',
      './app/bachillerato/*.scss',
      './app/bachillerato/**/*.scss'
    ],
    includePaths: [
      './',
    ],
    output: './static/app/css'
  }
});
from rest_framework import serializers
from .models import MultipleChoiceQuestion, Answer, MultipleChoiceQuestionSitting

class AnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = '__all__'

class MultipleChoiceQuestionSittingSerializer(serializers.ModelSerializer):
    class Meta:
        model = MultipleChoiceQuestionSitting
        fields = '__all__'
        read_only_fields = ('is_right_answer',)

class MultipleChoiceQuestionSerializer(serializers.ModelSerializer):
    answers = AnswerSerializer(many=True, read_only=True)
    sitting = serializers.SerializerMethodField()

    class Meta:
        model = MultipleChoiceQuestion
        fields = '__all__'

    def get_sitting(self, obj):
        user = self.context.get('user', None)
        if user and hasattr(user, 'studentprofile'):
            try:
                sitting = obj.sittings.get(student=user.studentprofile)
                return sitting.id
            except:
                return None
        else:
            return None

from django.contrib import admin
from .models import MultipleChoiceQuestion, Answer, MultipleChoiceQuestionSitting

class AnswerInline(admin.TabularInline):
	model = Answer

class MultipleChoiceQuestionAdmin(admin.ModelAdmin):
	inlines = [AnswerInline]

admin.site.register(MultipleChoiceQuestion, MultipleChoiceQuestionAdmin)
admin.site.register(MultipleChoiceQuestionSitting)
# Register your models here.

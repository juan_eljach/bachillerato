from django.shortcuts import render
from rest_framework import generics
from rest_framework.response import Response
from bachillerato.mixins import SlugManagerMixin
from courses.permissions import IsEnrolledOnSubject, CanAccessCourse
from .models import MultipleChoiceQuestion, MultipleChoiceQuestionSitting
from .permissions import IsSittingOwner
from .serializers import MultipleChoiceQuestionSerializer ,MultipleChoiceQuestionSittingSerializer

class QuestionsListView(SlugManagerMixin, generics.ListAPIView):
    queryset = MultipleChoiceQuestion.objects.all()
    permission_classes = (CanAccessCourse,)
    serializer_class = MultipleChoiceQuestionSerializer

    def get_queryset(self):
        user = self.request.user
        if hasattr(user, 'studentprofile'):
            course = self.get_course()
            student = user.studentprofile
            subjects_with_enrollment = student.enrollments.filter(subject__course=course)
            subjects_list = [s.subject for s in subjects_with_enrollment]
            q = self.queryset.filter(subject__in=subjects_list)
            return q

    def get_serializer(self, *args, **kwargs):
        user = self.request.user
        serializer_class = self.get_serializer_class()
        context = self.get_serializer_context()
        context.update({'user':user})
        kwargs['context'] = context
        serializer = serializer_class(*args, **kwargs)
        return serializer

class SittingDetailView(SlugManagerMixin, generics.RetrieveAPIView):
    queryset = MultipleChoiceQuestionSitting.objects.all()
    serializer_class = MultipleChoiceQuestionSittingSerializer
    lookup_field = 'pk'
    lookup_url_kwarg = 'pk'
    permission_classes = (IsSittingOwner,)


# Create your views here.
class MCQuestionSittingCreateView(generics.CreateAPIView):
    serializer_class = MultipleChoiceQuestionSittingSerializer

    def create(self, request, *args, **kwargs):
        print ("Entered create")
        user = request.user
        if hasattr(user, 'studentprofile'):
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            question = serializer.validated_data.get('mc_question')
            if hasattr(question, 'sitting'):
                raise serializers.ValidationError('The user already answered this question')
            else:
                pass
            given_answer = serializer.validated_data.get('given_answer')
            subject_permission = IsEnrolledOnSubject()
            subject = question.subject
            can_answer_question = subject_permission.has_object_permission(request, subject_permission, subject)
            if can_answer_question:
                if given_answer.question == question:
                    print ("Given answer is correct? {}".format(given_answer.correct))
                    is_right_answer = given_answer.correct
                    print ("About to perform create")
                    self.perform_create(serializer, student=user.studentprofile, is_right_answer=is_right_answer)
                    return Response(serializer.data)
            else:
                raise exceptions.PermissionDenied
        else:
            raise serializers.ValidationError('The user has not profile')
            #self.perform_create(serializer)

    def perform_create(self, serializer, *args, **kwargs):
        print ("Entered perform create ")
        serializer.save(**kwargs)

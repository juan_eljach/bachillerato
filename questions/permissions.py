from rest_framework import permissions

class IsSittingOwner(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        user = request.user
        if hasattr(user, 'studentprofile'):
            return obj.student == user.studentprofile

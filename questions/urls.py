from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import QuestionsListView, MCQuestionSittingCreateView, SittingDetailView

#/api/questions/

urlpatterns = [
    path('course/<slug:course_slug>/', QuestionsListView.as_view()),
    path('sitting/<int:pk>/', SittingDetailView.as_view()),
    path('checkanswer/', MCQuestionSittingCreateView.as_view()),
]

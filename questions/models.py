from django.db import models
from accounts.models import StudentProfile
from courses.models import Subject

class MultipleChoiceQuestion(models.Model):
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE, blank=False, null=False, related_name='questions')
    title = models.CharField(max_length=50, blank=True, null=True)
    content = models.TextField()
    explanation = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.content[:50]

#Answer Model for MultipleChoiceQuestion
class Answer(models.Model):
    question = models.ForeignKey(MultipleChoiceQuestion, on_delete=models.CASCADE, blank=False, null=False, related_name='answers')
    content = models.TextField()
    correct = models.BooleanField(blank=False, default=False)

class MultipleChoiceQuestionSitting(models.Model):
    student = models.ForeignKey(StudentProfile, on_delete=models.CASCADE, blank=False, null=False, related_name='questions_sittings')
    mc_question = models.ForeignKey(MultipleChoiceQuestion, on_delete=models.CASCADE, blank=False, null=False, related_name="sittings")
    given_answer = models.ForeignKey(Answer, on_delete=models.CASCADE, blank=True, null=True)
    is_right_answer = models.BooleanField(default=False)
    timestamp = models.DateTimeField(auto_now_add=True)

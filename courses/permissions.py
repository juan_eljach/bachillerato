from rest_framework import permissions
from rest_framework import exceptions
from django.shortcuts import get_object_or_404
from .models import Course

class CanAccessCourse(permissions.BasePermission):
    def has_permission(self, request, view):
        course_slug = view.kwargs.get('course_slug')
        course = get_object_or_404(Course, slug__iexact=course_slug)
        user = request.user
        if hasattr(user, 'studentprofile'):
            student = user.studentprofile
            enrollments = student.enrollments.filter(subject__course=course)
            if enrollments.exists() or student.current_grade == course:
                return True
        else:
            return False

class IsEnrolledOnSubject(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        user = request.user
        if hasattr(user, 'studentprofile'):
            enrollments = obj.subject_enrollment.filter(user_profile=user.studentprofile)
            return enrollments.exists()

class CanAccessModule(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        user = request.user
        if hasattr(user, 'studentprofile'):
            enrollments = obj.subject.subject_enrollment.filter(user_profile=user.studentprofile)
            if enrollments.exists():
                return True
            elif obj.free_access:
                return user.studentprofile.current_grade == obj.subject.course
            else:
                return False

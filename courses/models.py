from hashlib import sha1
from django.conf import settings
from django.db import models
from django.template import defaultfilters
from bachillerato.choices import GRADE_CHOICES, PAYMENT_METHOD_CHOICES
from accounts.models import StudentProfile, TeacherProfile
from materials.models import Video, Document, Text, Audio

# Create your models here.
class Course(models.Model):
    name = models.CharField(max_length=50, choices=GRADE_CHOICES, unique=True)
    image = models.ImageField(upload_to='uploads', blank=True, null=True)
    price = models.IntegerField(blank=True, null=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, blank=True, null=True)
    slug = models.SlugField(max_length=60, unique=True)
    title = models.CharField(max_length=100, blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.get_name_display()

    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = defaultfilters.slugify(self.name)
            while True:
                try:
                    course_exists = Course.objects.get(slug__iexact=self.slug)
                    if course_exists:
                        self.slug = "{0}-{1}".format(self.slug, sha1(str(random.random()).encode('utf-8')).hexdigest()[:5])
                except Course.DoesNotExist:
                    break

        super(Course, self).save(*args, **kwargs)

class Subject(models.Model):
    name = models.CharField(max_length=30)
    course = models.ForeignKey(Course, on_delete=models.CASCADE, blank=False, null=False, related_name='subjects')
    image = models.ImageField(upload_to='uploads')
    teachers = models.ManyToManyField(TeacherProfile, blank=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, blank=True, null=True)
    hourly_intensity = models.IntegerField(blank=True, null=True, help_text="Intensidad Horaria a mostrar en certificados")
    last_update = models.DateTimeField(auto_now=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    slug = models.SlugField(max_length=60, unique=True)

    def __str__(self):
        return ("{0} - {1}".format(self.name, self.course))

    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = defaultfilters.slugify(self.name)
            while True:
                try:
                    subject_exists = Subject.objects.get(slug__iexact=self.slug)
                    if subject_exists:
                        self.slug = "{0}-{1}".format(self.slug, sha1(str(random.random()).encode('utf-8')).hexdigest()[:5])
                except Subject.DoesNotExist:
                    break
        super(Subject, self).save(*args, **kwargs)

class Module(models.Model):
    title = models.CharField(max_length=50)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE, blank=False, null=False, related_name='modules')
    last_update = models.DateTimeField(auto_now=True)
    timestamp =  models.DateTimeField(auto_now_add=True)
    videos = models.ManyToManyField(Video, blank=True)
    documents = models.ManyToManyField(Document, blank=True)
    texts = models.ManyToManyField(Text, blank=True)
    audios = models.ManyToManyField(Audio, blank=True)
    free_access = models.BooleanField()
    slug = models.SlugField(max_length=60, unique=True)

    def __str__(self):
        return ("{0}-{1}-{2}".format(self.title, self.subject.name, self.subject.course.name))

    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = defaultfilters.slugify(self.title)
            while True:
                try:
                    module_exists = Module.objects.get(slug__iexact=self.slug)
                    if module_exists:
                        self.slug = "{0}-{1}".format(self.slug, sha1(str(random.random()).encode('utf-8')).hexdigest()[:5])
                except Module.DoesNotExist:
                    break
        super(Module, self).save(*args, **kwargs)


class Enrollment(models.Model):
    user_profile = models.ForeignKey(StudentProfile, on_delete=models.CASCADE, related_name='enrollments')
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE, related_name='subject_enrollment')
    payment_method = models.CharField(max_length=30, choices=PAYMENT_METHOD_CHOICES, blank=True, null=True)
    paid_price = models.CharField(max_length=10, blank=True, null=True)
    payment_date = models.DateTimeField(blank=True, null=True)
    subject_approved = models.BooleanField(default=False)
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ('user_profile', 'subject')

    def __str__(self):
        return ("{0} - {1}".format(self.user_profile.user.get_full_name(), self.subject))

"""
Agregarle el campo de metodo de pago
Fecha de compra
Precio de pago (hacer otro campo de pago y registrar el valor de la compra)
"""


class EntryLog(models.Model):
    user_profile = models.ForeignKey(StudentProfile, on_delete=models.CASCADE)
    module = models.ForeignKey(Module, on_delete=models.CASCADE, related_name="entry_log")
    consumed_videos = models.ManyToManyField(Video, blank=True)
    consumed_documents = models.ManyToManyField(Document, blank=True)
    consumed_texts = models.ManyToManyField(Text, blank=True)
    consumed_audios = models.ManyToManyField(Audio, blank=True)

    def __str__(self):
        return ('Log: {0} - {1}'.format(self.user_profile, self.module))

    def get_percentage_completed(self):
        total_materials = float(self.module.videos.count() + self.module.texts.count() + self.module.documents.count() + self.module.audios.count())
        total_consumed_materials = float(self.consumed_videos.count() + self.consumed_documents.count() + self.consumed_texts.count() + self.consumed_audios.count())
        try:
            percentage = round(float((total_consumed_materials * 100) / total_materials), 0)
        except ZeroDivisionError:
            percentage = 0
        return percentage

    class Meta:
        unique_together = ('user_profile', 'module')

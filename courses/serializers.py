from rest_framework import serializers
from activities.serializers import MultipleChoiceQuestionSerializer, DictationSerializer, RelationshipQuestionSerializer
from exams.models import Sitting
from exams.serializers import SittingReportSerializer, ExamDetailSerializer
from materials.serializers import VideoSerializer, DocumentSerializer, TextSerializer, AudioSerializer
from .models import Course, Subject, Module, EntryLog, Enrollment

class EntryLogSerializer(serializers.ModelSerializer):
    percentage_completed = serializers.SerializerMethodField()
    class Meta:
        model = EntryLog
        fields = '__all__'

    def get_percentage_completed(self, obj):
        return obj.get_percentage_completed()

class ModuleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Module
        fields = '__all__'

class ModuleDetailSerializer(serializers.ModelSerializer):
    videos = VideoSerializer(many=True, read_only=True)
    documents = DocumentSerializer(many=True, read_only=True)
    texts = TextSerializer(many=True, read_only=True)
    audios = AudioSerializer(many=True, read_only=True)
    mc_questions = MultipleChoiceQuestionSerializer(many=True, read_only=True)
    dictations = DictationSerializer(many=True, read_only=True)
    relationships = RelationshipQuestionSerializer(many=True, read_only=True)
    entry_log = EntryLogSerializer(many=True, read_only=True)

    class Meta:
        model = Module
        fields = '__all__'

class SubjectSerializerMixin(serializers.Serializer):
    percentage_completed = serializers.SerializerMethodField()
    current_grade = serializers.SerializerMethodField()
    enrolled = serializers.SerializerMethodField()

    def get_percentage_completed(self, obj):
        total_percentage = None
        user = self.context.get('user', None)
        if user:
            if hasattr(user, 'studentprofile'):
                total_number_of_modules = obj.modules.all().count()
                modules_with_sitting_counter = 0
                for module in obj.modules.filter(exam__isnull=False):
                    try:
                        sitting = Sitting.objects.get(exam=module.exam, user_profile=user.studentprofile)
                        modules_with_sitting_counter += 1
                    except Sitting.DoesNotExist:
                        pass
                try:
                    total_percentage = round(float(float(modules_with_sitting_counter * 100) / total_number_of_modules), 0)
                except ZeroDivisionError:
                    total_percentage = 0
        return total_percentage

    def get_current_grade(self, obj):
        grades_list = []
        current_grade = 0
        user = self.context.get('user', None)
        if user:
            if hasattr(user, 'studentprofile'):
                for module in obj.modules.filter(exam__isnull=False):
                    try:
                        sitting = Sitting.objects.get(exam=module.exam, user_profile=user.studentprofile)
                        if sitting.completed:
                            grades_list.append(sitting.grade)
                    except Sitting.DoesNotExist:
                        pass
                if len(grades_list) > 0:
                    current_grade = float(float(sum(grades_list)) / len(grades_list))
        return current_grade

    def get_enrolled(self, obj):
        user = self.context.get('user', None)
        if user:
            enrollments = obj.subject_enrollment.filter(user_profile=user.studentprofile)
            return enrollments.exists()
        else:
            return None

class SubjectSerializer(SubjectSerializerMixin, serializers.ModelSerializer):
    class Meta:
        model = Subject
        fields = '__all__'


class SubjectDetailSerializer(serializers.ModelSerializer):
    modules = ModuleSerializer(many=True, read_only=True)
    subject_exam = serializers.PrimaryKeyRelatedField(read_only=True)
    class Meta:
        model = Subject
        fields = '__all__'

class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = '__all__'

class CourseDetailSerializer(serializers.ModelSerializer):
    subjects = SubjectSerializer(many=True, read_only=True)
    class Meta:
        model = Course
        fields = ('name', 'subjects')

class AdvanceSerializer(serializers.Serializer):
    percentage_completed = serializers.FloatField()

class ModuleReportSerializer(serializers.ModelSerializer):
    exam_sitting = SittingReportSerializer(source='exam.sitting', read_only=True)
    class Meta:
        model = Module
        fields = ('title', 'exam_sitting')

class SubjectReportSerializer(SubjectSerializerMixin, serializers.ModelSerializer):
    modules = ModuleReportSerializer(many=True, read_only=True)
    class Meta:
        model = Subject
        fields = ('percentage_completed', 'current_grade', 'modules')

class EnrollmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Enrollment
        fields = '__all__'

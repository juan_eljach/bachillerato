from django.shortcuts import render, get_object_or_404
from rest_framework import viewsets, generics
from rest_framework.views import APIView
from rest_framework.response import Response
from bachillerato.mixins import SlugManagerMixin
from exams.models import Sitting
from .models import Course, Subject, Module, EntryLog
from .permissions import CanAccessModule, CanAccessCourse
from .serializers import CourseSerializer, CourseDetailSerializer, SubjectSerializer, SubjectDetailSerializer, ModuleSerializer, ModuleDetailSerializer, EntryLogSerializer, AdvanceSerializer, SubjectReportSerializer

class CourseViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer
    lookup_field = 'slug'
    lookup_url_kwarg = 'course_slug'
    http_method_names = ['get', 'head', 'options']

#    def get_queryset(self):
#        queryset = self.queryset
#        user = self.request.user
#        if hasattr(user, 'studentprofile'):
#            grade = user.studentprofile.current_grade
#            if grade:
#                queryset = self.queryset.filter(id=grade.id)
#        return queryset

    def get_serializer_class(self, *args, **kwargs):
        if self.action == 'list':
            return CourseSerializer
        if self.action == 'retrieve':
            return CourseDetailSerializer
        return self.serializer_class

#    def get_permissions(self):
#        if self.action == 'retrieve':
#            self.permission_classes = [CanAccessCourse]
#        return super(CourseViewSet, self).get_permissions()

class SubjectViewSet(SlugManagerMixin, viewsets.ReadOnlyModelViewSet):
    queryset = Subject.objects.all()
    serializer_class = SubjectSerializer
    lookup_field = 'slug'
    lookup_url_kwarg = 'subject_slug'

    def get_queryset(self):
        course = self.get_course()
        user = self.request.user
        filtered_qs = self.queryset.filter(course=course)
        return filtered_qs

    def get_serializer_class(self, *args, **kwargs):
        if self.action == 'list':
            return SubjectSerializer
        if self.action == 'retrieve':
            return SubjectDetailSerializer
        return self.serializer_class

    def get_serializer(self, *args, **kwargs):
        user = self.request.user
        serializer_class = self.get_serializer_class()
        context = self.get_serializer_context()
        if self.action == 'list':
            context.update({'user':user})
            kwargs['context'] = context
        serializer = serializer_class(*args, **kwargs)
        return serializer



class ModuleViewSet(SlugManagerMixin, viewsets.ReadOnlyModelViewSet):
    queryset = Module.objects.all()
    serializer_class = ModuleSerializer
    lookup_field = 'slug'
    lookup_url_kwarg = 'module_slug'

    def get_queryset(self):
        subject = self.get_subject()
        filtered_qs = self.queryset.filter(subject=subject)
        return filtered_qs

    def get_permissions(self):
        if self.action == 'retrieve':
            self.permission_classes = [CanAccessModule]
        return super(ModuleViewSet, self).get_permissions()

    def get_serializer_class(self, *args, **kwargs):
        print (self.action)
        if self.action == 'list':
            return ModuleSerializer
        if self.action == 'retrieve':
            return ModuleDetailSerializer
        return self.serializer_class

    def get_serializer(self, *args, **kwargs):
        user = self.request.user
        serializer_class = self.get_serializer_class()
        context = self.get_serializer_context()
        if self.action == 'retrieve':
            obj = self.get_object()
            if hasattr(user, 'studentprofile'):
                entry_log, created = EntryLog.objects.get_or_create(module=obj, user_profile=user.studentprofile)
            else:
                entry_log = None
            context.update({'entry_log':entry_log})
            kwargs['context'] = context
        serializer = serializer_class(*args, **kwargs)
        return serializer


class GetAdvanceBySubject(SlugManagerMixin, APIView):
    def get_advance_percentage(self, user, subject):
        total_percentage = 0
        grades_list = []
        current_grade = 0
        if hasattr(user, 'studentprofile'):
            total_number_of_modules = subject.modules.all().count()
            modules_with_sitting_counter = 0
            for module in subject.modules.filter(exam__isnull=False):
                try:
                    sitting = Sitting.objects.get(exam=module.exam, user_profile=user.studentprofile)
                    modules_with_sitting_counter += 1
                    if sitting.completed:
                        grades_list.append(sitting.grade)
                except Sitting.DoesNotExist:
                    pass
            total_percentage = round(float(float(modules_with_sitting_counter * 100) / total_number_of_modules), 0)
            current_grade = float(float(sum(grades_list)) / len(grades_list))
        data = {'percentage_completed':total_percentage, 'current_grade':current_grade}
        return data

    def get(self, request, format=None, *args, **kwargs):
        subject = self.get_subject()
        user = request.user
        data = self.get_advance_percentage(user, subject)
        #serializer = AdvanceSerializer(data={'percentage_completed':total_percentage})
        serializer = SubjectAdvanceSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data)
        return response

class GetAdvanceByCourse(SlugManagerMixin, APIView):
    def get_advance_percentage(self, user, course):
        total_percentage = 0
        if hasattr(user, 'studentprofile'):
            percentages_per_subject = []
            for subject in course.subjects.all():
                total_number_of_modules = subject.modules.all().count()
                modules_with_sitting_counter = 0
                for module in subject.modules.filter(exam__isnull=False):
                    try:
                        sitting = Sitting.objects.get(exam=module.exam, user_profile=user.studentprofile)
                        modules_with_sitting_counter += 1
                    except Sitting.DoesNotExist:
                        pass
                advance_percentage_per_subject = round(float(float(modules_with_sitting_counter * 100) / total_number_of_modules), 0)
                percentages_per_subject.append(advance_percentage_per_subject)
            total_percentage = round(float(sum(percentages_per_subject) / len(percentages_per_subject)), 0)
        return total_percentage

    def get(self, request, format=None, *args, **kwargs):
        course = self.get_course()
        user = request.user
        total_percentage = self.get_advance_percentage(user, course)
        serializer = AdvanceSerializer(data={'percentage_completed':total_percentage})
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data)


class EntryLogAPIView(generics.RetrieveAPIView):
    model_class = EntryLog
    queryset = EntryLog.objects.all()
    serializer_class = EntryLogSerializer
    lookup_field = 'module__slug'
    lookup_url_kwarg = 'module_slug'

class SubjectReportAPIView(generics.RetrieveAPIView):
    model_class = Subject
    queryset = Subject.objects.all()
    serializer_class = SubjectReportSerializer
    lookup_field = 'slug'
    lookup_url_kwarg = 'subject_slug'


class SubjectReportViewSet(SlugManagerMixin, viewsets.ReadOnlyModelViewSet):
    queryset = Subject.objects.all()
    serializer_class = SubjectReportSerializer
    lookup_field = 'slug'
    lookup_url_kwarg = 'subject_slug'

    def get_queryset(self):
        course = self.get_course()
        filtered_qs = self.queryset.filter(course=course)
        return filtered_qs

    def get_serializer(self, *args, **kwargs):
        user = self.request.user
        serializer_class = self.get_serializer_class()
        context = self.get_serializer_context()
        context.update({'user':user})
        kwargs['context'] = context
        serializer = serializer_class(*args, **kwargs)
        return serializer

#    def get_object(self):
#        print (self.request.user)
# Create your views here.

from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import CourseViewSet, SubjectViewSet, ModuleViewSet, EntryLogAPIView, GetAdvanceByCourse, GetAdvanceBySubject, SubjectReportViewSet#, GetSubjectsAdvance

urlpatterns = [
    path('courses/', CourseViewSet.as_view({'get':'list'})),
    path('courses/<slug:course_slug>/', CourseViewSet.as_view({'get':'retrieve'})),
    path('courses/<slug:course_slug>/advance/', GetAdvanceByCourse.as_view()),
    path('courses/<slug:course_slug>/subjects/', SubjectViewSet.as_view({'get':'list'})),
#    path('courses/<slug:course_slug>/subjects/advance/', GetSubjectsAdvance.as_view()),
    path('courses/<slug:course_slug>/subjects/<slug:subject_slug>/', SubjectViewSet.as_view({'get':'retrieve'})),
    path('courses/<slug:course_slug>/subjects/<slug:subject_slug>/report/', SubjectReportViewSet.as_view({'get':'retrieve'})),
    path('courses/<slug:course_slug>/subjects/<slug:subject_slug>/advance/', GetAdvanceBySubject.as_view()),
    path('courses/<slug:course_slug>/subjects/<slug:subject_slug>/modules/', ModuleViewSet.as_view({'get':'list'})),
    path('courses/<slug:course_slug>/subjects/<slug:subject_slug>/modules/<slug:module_slug>/', ModuleViewSet.as_view({'get':'retrieve'})),
    path('courses/<slug:course_slug>/subjects/<slug:subject_slug>/modules/<slug:module_slug>/entrylog/', EntryLogAPIView.as_view()),
]

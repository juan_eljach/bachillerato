from django.contrib import admin
from .models import Course, Subject, Module, EntryLog, Enrollment

class CourseAdmin(admin.ModelAdmin):
    exclude = ("slug", "created_by")

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

class SubjectAdmin(admin.ModelAdmin):
    exclude = ("slug",)

class ModuleAdmin(admin.ModelAdmin):
    filter_horizontal = ('videos', 'texts', 'audios', 'documents',)
    exclude = ("slug",)

class EnrollmentAdmin(admin.ModelAdmin):
    search_fields = ['user_profile__user__email', 'user_profile__user__first_name', 'user_profile__user__last_name' ,'user_profile__identity']
    list_display = ['__str__', 'user_profile', 'subject', 'subject_approved', 'payment_method', 'paid_price', 'payment_date']
    exclude = ('subject_approved',)

admin.site.register(Enrollment, EnrollmentAdmin)
admin.site.register(Course, CourseAdmin)
admin.site.register(Subject, SubjectAdmin)
admin.site.register(Module, ModuleAdmin)
admin.site.register(EntryLog)

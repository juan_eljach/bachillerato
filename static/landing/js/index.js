/**
 * Theme JS
 */

'use strict';

/*** Preloader ***/

var preloader = (function() {

	// Variables
	var $window = $(window);
	var loader = $("#loader-wrapper");

	// Methods
	$window.on({
		'load': function() {
			loader.fadeOut();
		}
	});

	// Events

})();


/*** Navbar ***/

var navbar = (function() {

	// Variables
	var navbar = $('.navbar');
	var navbarLinks = navbar.find('.navbar-nav > li:not(.dropdown) > a');
	var navbarCollapse = $('.navbar-collapse');
	var scrollTop = $(window).scrollTop();

	// Methods
	// function makeInverse() {
	// 	navbar.removeClass('navbar-default').addClass('navbar-inverse');
	// }
	// function makeDefault() {
	// 	navbar.removeClass('navbar-inverse').addClass('navbar-default');
	// }

	// Events

	// Toggle navbar on page load if needed
	// if (scrollTop > 0) {
	// 	makeInverse();
	// }

	// Toggle navbar on scroll
	// $(window).scroll(function() {
	// 	scrollTop = $(window).scrollTop();

	// 	if (scrollTop > 0 && $('.navbar-default').length) {
	// 		makeInverse();
	// 	} else if (scrollTop === 0) {
	// 		makeDefault();
	// 	}
	// });

	// Toggle navbar on collapse
	navbarCollapse.on({
		'show.bs.collapse': function() {
			makeInverse();
		},
		'hidden.bs.collapse': function() {
			scrollTop = $(window).scrollTop();

			if (scrollTop === 0) { 
				makeDefault();
			}
		}
	});

	// Close collapsed navbar on click
	navbarLinks.on('click', function() {
		navbarCollapse.collapse('hide');
	});

})();

/*** Smooth scroll to anchor ***/

var smoothScroll = (function() {

	// Variables
	var link = $('a[href^="#section__"]');
	var duration = 1000;

	// Methods
	function scrollTo(link) {
		var target = $(link.attr('href'));
		var navbar = $('.navbar');
		var navbarHeight = navbar.outerHeight();

		if ( target.length ) {
			$('html, body').animate({
				scrollTop: target.offset().top - navbarHeight + 20
			}, duration);
		}
	}

	// Events
	link.on('click', function(e) {
		e.preventDefault();
		scrollTo( $(this) );
	});

})();


/*** Button to top page ***/

var toTopButton = (function() {

	// Variables
	var topButton = $('#back-to-top');
	var scrollTop = $(window).scrollTop();
	var isActive = false;
	if (scrollTop > 100) {
		isActive = true;
	}

	// Methods	

	// Events
	$(window).scroll(function() {
		scrollTop = $(window).scrollTop();

		if (scrollTop > 100 && !isActive) {
	        isActive = true;
	        topButton.fadeIn();
	    } else if (scrollTop <= 100 && isActive) {
	        isActive = false;
	        topButton.fadeOut();
	    }

	});

})();


/*** Search ***/

var search = (function() {

	// Variables
	var searchForm = $(".navbar-search");
	var searchToggle = $(".navbar-search__toggle");

	// Method
	function toggleSearch() {
		searchForm.toggle();
	}

	// Events
	searchToggle.on('click', function(e) {
		e.preventDefault();

		toggleSearch();
	});

})();

/*** Modals ***/

var modals = (function() {

	// Variables
	var $modals = $('.price__item .btn-primary');

	// Methods
	function toggleModals(elem) {
		var $this = elem;
		
		var target = $this.data('modal');
		var link = $this.data('link');
		
		var currentModal = $(target);
		var $payButton = currentModal.find('.modal-body__payu a');

		$payButton.attr('href', link);
		$payButton.attr('target', '_blank');

		$(target).modal('show');	

		currentModal.on('hidden.bs.modal', function() {			
			if (target.length) {
				$(target).modal('hide');	
			}
			target = '';
		});
	}

	// Events
	$modals.on('click', function() {
		toggleModals( $(this) );

		return false;
	});

})();


/*** AOS ***/

var aos = (function() {

	// Variables
	var $aos = $('[data-aos]');

	// Methods
	function init() {
		AOS.init({
			duration: 1000
		});
	}

	// Events
	if ( $aos.length ) {
		init();
	}

})();


/*** Mobile hover ***/

var mobileHover = (function() {

	// Variables
	var item = $('.teacher__item');

	// Methods
	function trigger(elem) {
		elem.trigger('hover');
	}

	// Events
	item.on({
		'touchstart': function() {
			trigger( $(this) );
		},
		'touchend': function() {
			trigger( $(this) );
		}
	});

})();


/*** Countdown ***/

var countdown = (function() {

	// Variables
	var clock = $('#clock');
	var toDate = '2017/10/09';

	// Methods
	function init() {
		clock.countdown(toDate, function(event) {
			$(this).html(event.strftime(''
				+ '<span>%D</span> days '
				+ '<span>%H</span> hr '
				+ '<span>%M</span> min '
				+ '<span>%S</span> sec'));
		});
	}

	// Events
	if ( clock.length ) {
		init();
	}

})();


/*** Countdown: Alt ***/

var countdown_alt = (function() {

	// Variables
	var clock = $('#event__timer');
	var toDate = '2017/10/09';

	// Methods
	function init() {
		clock.countdown(toDate, function(event) {
			$(this).html(event.strftime(''
				+ '<span>%D</span> days '
				+ '<span>%H</span> hr '
				+ '<span>%M</span> min '
				+ '<span>%S</span> sec'));
		});
	}

	// Events
	if ( clock.length ) {
		init();
	}

})();

/*** Count to ***/

var countTo = (function() {

	// Variables
	var statsItem = $('.stats_item__nbr');

	// Methods
	function init() {
		statsItem.each(function() {
			var $this = $(this);

			$this.waypoint(function(direction) {
				$this.not('.finished').countTo();
				}, {
				offset: 500
			});
		});
	}
	
	// Events
	if ( statsItem.length ) {
		init();
	}
	
})();


/*** Owl carousel ***/

var owl = (function() {

	// Variables
	var testimonialItem = $("#testimonials__carousel");
	var getResponsiveConfig = function(config) {
		var base = {
			items: 1,
			loop: true,
			slideSpeed: 1500,
			dots: true,
			autoplay: false,
			responsiveClass: true,
			nav: true,
			center: true,
			navText: ["<i class='ion-ios-arrow-left'></i>", "<i class='ion-ios-arrow-right'></i>"],
		}

		return Object.assign(base, config);
	} 

	// Methods
	function init() {		
		testimonialItem.owlCarousel({						
			responsive: {
				0: getResponsiveConfig({items: 1}),
				768: getResponsiveConfig({items: 2}),
				1024: getResponsiveConfig({items: 3})
			},			
		});
	}
	
	// Events
	if ( testimonialItem.length ) {
		init();
	}
	
})();


/*** DatePicker ***/
var items = $('[data-toggle="datepicker"]');

if (items.length) {
	$('[data-toggle="datepicker"]').datepicker({
		language: 'es-ES',
		autoHide: true,
		endDate: Date.now(),
		format: 'yyyy-mm-dd'
	});
}

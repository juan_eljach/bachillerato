const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
	mode: 'development',
	devServer: {
		port: 3000,
		hot: true,
		open: true,
		proxy: [
			{
				context: ['/static', '/api', '/', '/media'],
				target: 'http://staging.bachilleratoonline.co',
				changeOrigin: true
			}
		]
	},
	devtool: 'source-map',
	resolve: {
		modules: [path.resolve(process.cwd(), 'app'), path.resolve(process.cwd(), 'node_modules')],
		alias: {
			xhr: path.resolve(process.cwd(), 'app/core/xhr')
		}
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /(node_modules)/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['env', 'react'],
						plugins: ['transform-object-rest-spread', 'transform-class-properties', 'transform-runtime']
					}
				}
			},
			{
				test: /\.s?css$/,
				exclude: /(node_modules)/,
				use: ['style-loader', 'css-loader', 'sass-loader']
			}
		]
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: path.resolve(__dirname, 'templates/app.html')
		}),
		new webpack.HotModuleReplacementPlugin()
	]
};

const getConfig = require('./utils/getConfig');

module.exports = ({ config, mode } = {mode: 'development'}) => {
  return getConfig(config, mode);
};

const webpackMerge = require('webpack-merge');

/**
 * This utility allows you to obtain the configuration according to the mode and presets inidcated.
 * @param {object} - config, mode, and presets to load.
 */
module.exports = (config, mode, presets) => {
  const currentConfig = require('../configs')[config];
  const modeConfig = require(`../webpack.${mode}`);

  return webpackMerge(
    {},
    currentConfig,
    modeConfig
  );
};

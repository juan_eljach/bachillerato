const path = require('path');
const cwd = process.cwd();

module.exports = {
  bachillerato: {
    entry: {
      'bachillerato': path.resolve(cwd, 'app/bachillerato/index.js')
    },
    output: {
      path: path.resolve(cwd, 'static/app/js/'),
      filename: '[name].js',
    }
  }
};

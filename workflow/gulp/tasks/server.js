const gulp = require('gulp');
const browserSync = require('browser-sync');

const server = (config) => {
  gulp.task('server', (done) => {
    browserSync.init(config.server);

    done();
  }); 
}

const reload = (config) => {
  gulp.task('reload', (done) => {
    browserSync.reload();

    done();
  });
}

module.exports = {
  server,
  reload
};
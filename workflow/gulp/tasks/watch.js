const gulp = require('gulp');
const browserSync = require('browser-sync');

const watch = (config) => {
  // Register gulp watch
  gulp.task('watch', () => {    
    for (const w of config.watch) {      
      gulp.watch(config[w.from].files, w.tasks);
    }
  });
}

module.exports = watch;
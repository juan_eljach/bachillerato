const serverTasks = require('./server');
const watch = require('./watch');

module.exports = {
  ...serverTasks,
  watch
};
const path = require('path');
const cwd = process.cwd();

module.exports = {
	tasks: ['server', 'reload', 'watch'],
	server: {
		proxy: {
			target: 'http://18.222.24.99'
		}
	},
	templates: {
		files: [path.resolve(cwd, '../templates/**/*.html')]
	},
	scripts: {
		files: [path.resolve(cwd, '../static/landing/js/*.js'), path.resolve(cwd, '../static/landing/js/**/*.js')]
	},
	css: {
		files: [path.resolve(cwd, '../static/landing/css/*.css'), path.resolve(cwd, '../static/landing/css/**/*.css')]
	},
	watch: [
		{ from: 'templates', tasks: ['reload'] },
		{ from: 'css', tasks: ['reload'] },
		{ from: 'scripts', tasks: ['reload'] }
	]
};

const gulp = require('gulp');
const tasks = require('./tasks');

const env = process.env.NODE_ENV;

// Current config
const config = require(`./configs/${process.env.CONFIG}.js`);

// Register gulp tasks
for (const task of config.tasks) {
  tasks[task](config);
}

gulp.task('default', config.tasks);
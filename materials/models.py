from django.db import models
from ckeditor.fields import RichTextField
from urllib.parse import urlparse, parse_qs
from accounts.models import StudentProfile
from django.template import defaultfilters
from hashlib import sha1


VIDEO_TYPE_CHOICES = (
    ("yt", "Youtube"),
    ("vm", "Vimeo")
)
class Video(models.Model):
    title = models.CharField(max_length=60)
    type = models.CharField(max_length=5, choices=VIDEO_TYPE_CHOICES)
    link = models.URLField()
    description = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)
    slug = models.SlugField(max_length=100)

    def __str__(self):
        return self.title

    def get_video_url(self):
        if self.type=='yt':
            """
            Examples:
            - http://youtu.be/SA2iWivDJiE
            - http://www.youtube.com/watch?v=_oPAwA_Udwc&feature=feedu
            - http://www.youtube.com/embed/SA2iWivDJiE
            - http://www.youtube.com/v/SA2iWivDJiE?version=3&amp;hl=en_US
            """
            video_id=''
            query = urlparse(self.link)
            if query.hostname == 'youtu.be':
                video_id=query.path[1:]
            if query.hostname in ('www.youtube.com', 'youtube.com'):
                if query.path == '/watch':
                    p = parse_qs(query.query)
                    video_id=p['v'][0]
                if query.path[:7] == '/embed/':
                    video_id=query.path.split('/')[2]
                if query.path[:3] == '/v/':
                    video_id=query.path.split('/')[2]
            return 'https://www.youtube.com/embed/%s' % video_id

        if self.type=='vm':
            return self.link
        return None

    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = defaultfilters.slugify(self.title)
            try:
                video_exists = Video.objects.get(slug__iexact=self.slug)
                if video_exists:
                    self.slug = "{0}-{1}".format(self.slug, sha1(str(random.random()).encode('utf-8')).hexdigest()[:5])
            except Video.DoesNotExist:
                super(Video, self).save(*args, **kwargs)

class Document(models.Model):
    title = models.CharField(max_length=60)
    link = models.URLField(blank=True)
    document = models.FileField(upload_to="documents", blank=True, null=True)
    description = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)
    slug = models.SlugField(max_length=100)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = defaultfilters.slugify(self.title)
            try:
                doc_exists = Document.objects.get(slug__iexact=self.slug)
                if doc_exists:
                    self.slug = "{0}-{1}".format(self.slug, sha1(str(random.random()).encode('utf-8')).hexdigest()[:5])
            except Document.DoesNotExist:
                super(Document, self).save(*args, **kwargs)

class Text(models.Model):
    title = models.CharField(max_length=60)
    content = RichTextField()
    timestamp = models.DateTimeField(auto_now_add=True)
    slug = models.SlugField(max_length=100)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = defaultfilters.slugify(self.title)
            try:
                text_exists = Text.objects.get(slug__iexact=self.slug)
                if text_exists:
                    self.slug = "{0}-{1}".format(self.slug, sha1(str(random.random()).encode('utf-8')).hexdigest()[:5])
            except Text.DoesNotExist:
                super(Text, self).save(*args, **kwargs)

class Audio(models.Model):
    title = models.CharField(max_length=60)
    description = models.TextField()
    file = models.FileField(upload_to='audios', blank=True, null=True)

#    def get_percentage_completed(self):

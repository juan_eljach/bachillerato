from django.contrib import admin
from .models import Video, Document, Text, Audio

class VideoAdmin(admin.ModelAdmin):
    exclude = ("slug",)

class DocumentAdmin(admin.ModelAdmin):
    exclude = ("slug",)

class TextAdmin(admin.ModelAdmin):
    exclude = ("slug",)

admin.site.register(Video, VideoAdmin)
admin.site.register(Document, DocumentAdmin)
admin.site.register(Text, TextAdmin)
admin.site.register(Audio)
# Register your models here.

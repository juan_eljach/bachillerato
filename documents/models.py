import datetime
from django.db import models
from accounts.models import StudentProfile
from courses.models import Course
from bachillerato.choices import STUDENT_DOCUMENT_CHOICES, CYCLE_GRADE_CHOICES


class Document(models.Model):
    student = models.ForeignKey(StudentProfile, on_delete=models.CASCADE, blank=False, null=False)
    type = models.CharField(max_length=20, choices=STUDENT_DOCUMENT_CHOICES)
    document = models.FileField()
    timestamp = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)

class Certificate(models.Model):
    course = models.ForeignKey(Course, on_delete=models.SET_NULL, blank=True, null=True)
    student = models.ForeignKey(StudentProfile, on_delete=models.CASCADE, blank=False, null=False)
    timestamp = models.DateTimeField(auto_now_add=True)
    cycle = models.CharField(max_length=10, choices=CYCLE_GRADE_CHOICES, blank=True, null=True)

    def __str__(self):
        return ('{0} - {1}'.format(self.student.user.get_full_name(), self.course))

    def save(self, *args, **kwargs):
        if not self.pk:
            if self.course.name == 'sixth' or self.course.name == 'seventh':
                self.cycle = 'three'
            elif self.course.name == 'eighth' or self.course.name == 'ninth' or self.course.name == 'basic':
                self.cycle = 'four'
            elif self.course.name == 'tenth':
                self.cycle = 'five'
            elif self.course.name == 'eleven' or self.course.name == 'middle':
                self.cycle = 'six'
        super(Certificate, self).save(*args, **kwargs)

class ConstancyTemplate(models.Model):
    text = models.TextField()

    def __str__(self):
        return "{}...".format(self.text[0:15])

class Constancy(models.Model):
    student_name = models.CharField(max_length=60)
    identity = models.PositiveIntegerField()
    expedition_place = models.CharField(max_length=50, blank=True, null=True)
    text = models.ForeignKey(ConstancyTemplate, on_delete=models.SET_NULL, blank=True, null=True)
    custom_text = models.TextField(blank=True, null=True)
    reference_code = models.CharField(max_length=10, blank=True, null=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.student_name

    def save(self, **kwargs):
        if not self.pk:
            last = Constancy.objects.last()
            today = datetime.date.today()
            if last:
                new_id = last.id + 1
            else:
                new_id = 1
            self.reference_code = "{}-{}".format(new_id, today.year)
            if self.text:
                self.custom_text = None
            else:
                pass
        super(Constancy, self).save(**kwargs)

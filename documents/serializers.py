from rest_framework import serializers
from .models import Document, Certificate, Constancy

class DocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Document
        fields = '__all__'

class CertificateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Certificate
        fields = '__all__'
        read_only_fields = ('timestamp', 'cycle')

class ConstancySerializer(serializers.ModelSerializer):
    class Meta:
        model = Constancy
        fields = '__all__'
        read_only_fields = ('reference_code',)

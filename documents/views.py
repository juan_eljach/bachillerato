from weasyprint import HTML, CSS
from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.template import RequestContext
from django.template.loader import get_template
from rest_framework import viewsets, serializers, generics
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from bachillerato.choices import COURSES_IN_BASIC, COURSES_IN_MIDDLE
from bachillerato.mixins import SlugManagerMixin
from courses.models import Enrollment
from exams.models import Sitting
from .choices import FAKE_SUBJECTS_IN_BASIC, FAKE_SUBJECTS_IN_MIDDLE, DEFAULT_PERFORMANCE_FOR_FAKE_SUBJECTS, DEFAULT_PERFORMANCE_FOR_ADDITIONAL_SUBJECTS, ADDITIONAL_SUBJECTS
from .serializers import DocumentSerializer, CertificateSerializer, ConstancySerializer
from .models import Document, Certificate, Constancy

from django.contrib.staticfiles.templatetags.staticfiles import static


class DocumentListAPIView(generics.ListAPIView):
    queryset = Document.objects.all()
    serializer_class = DocumentSerializer

    def get_queryset(self):
        studentprofile = self.request.user.studentprofile
        queryset = self.queryset
        return queryset.filter(student=studentprofile)

class CertificateViewSet(SlugManagerMixin, viewsets.ModelViewSet):
    queryset = Certificate.objects.all()
    serializer_class = CertificateSerializer
    permission_classes = [IsAdminUser]

    def get_queryset(self):
        user = self.request.user
        filtered_qs = self.queryset.filter(student=user.studentprofile)
        return filtered_qs

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        course = serializer.validated_data.get('course')
        student = serializer.validated_data.get('student')
        filtered_enrollments = Enrollment.objects.filter(user_profile=student, subject__course=course, subject_approved=True)
        if filtered_enrollments.count() == course.subjects.all().count():
            self.perform_create(serializer)
            return Response(serializer.data)
        else:
            raise serializers.ValidationError('The user has not approved all the subjects in the Course')

class ConstacyCreateView(generics.CreateAPIView):
    serializer_class = ConstancySerializer

def calculate_number(x):
    x = x*0.1
    x = round(x, 2)
    return x

def find_performance(grade):
    performances_dict = {
        'low': [calculate_number(x) for x in range(10, 30)],
        'basic': [calculate_number(x) for x in range(30, 40)],
        'high': [calculate_number(x) for x in range(40, 46)],
        'superior': [calculate_number(x) for x in range(46, 51)]
    }
    for k,v in performances_dict.items():
        if grade in v:
            return k

def download_certificate(request, pk):
    performance_choices = {
        'low': 'DESEMPEÑO BAJO',
        'basic': 'DESEMPEÑO BÁSICO',
        'high': 'DESEMPEÑO ALTO',
        'superior': 'DESEMPEÑO SUPERIOR'
    }
    html_template = get_template("documents/certificate_template.html")
    certificate = get_object_or_404(Certificate, pk=pk)
    student = certificate.student
    student_full_name = student.user.get_full_name()
    identity = student.identity
    cycle = certificate.cycle
    year = student.semester[0:4]
    semester_simple = student.semester[5:7]
    subjects_dict = {}
    for subject in certificate.course.subjects.all():
        sitting = get_object_or_404(Sitting, user_profile=student, exam__subject=subject)
        grade = sitting.grade
        performance_key = find_performance(grade)
        performance_display = performance_choices[performance_key]
        subjects_dict[subject.name] = {'performance':performance_display, 'hours': subject.hourly_intensity}
    if certificate.course.name in COURSES_IN_BASIC:
        for k,v in FAKE_SUBJECTS_IN_BASIC.items():
            v['performance'] = DEFAULT_PERFORMANCE_FOR_FAKE_SUBJECTS
            subjects_dict[k] = v
    elif course.name in COURSES_IN_MIDDLE:
        for k,v in FAKE_SUBJECTS_IN_MIDDLE.items():
            v['performance'] = DEFAULT_PERFORMANCE_FOR_FAKE_SUBJECTS
            subjects_dict[k] = v
    else:
        pass
    for k,v in ADDITIONAL_SUBJECTS.items():
        v['performance'] = DEFAULT_PERFORMANCE_FOR_ADDITIONAL_SUBJECTS
        subjects_dict[k] = v
    data_dict = {
        'pagesize':"A4",
        'certificate':certificate,
        'student': certificate.student,
        'student_full_name': student.user.get_full_name(),
        'year':year,
        'semester_simple':semester_simple,
        'subjects':subjects_dict
    }

    rendered_html = html_template.render(data_dict, request).encode(encoding="UTF-8")
    css_url = static('documents/css/certificate.css')
    pdf_file = HTML(string=rendered_html).write_pdf()
    http_response = HttpResponse(pdf_file, content_type='application/pdf')
    http_response['Content-Disposition'] = 'filename="certificado.pdf"'
    return http_response

def download_constancy(request, pk):
    html_template = get_template("documents/constancy_template.html")
    constancy = get_object_or_404(Constancy, pk=pk)
    if constancy.text:
        render_text = constancy.text.text
    else:
        render_text = constancy.custom_text
    data_dict = {
        'pagesize':"A4",
        'student_name':constancy.student_name,
        'identity': constancy.identity,
        'expedition_place': constancy.expedition_place,
        'render_text': render_text,
        'reference_code': constancy.reference_code,
        'timestamp': constancy.timestamp,
    }

    rendered_html = html_template.render(data_dict, request).encode(encoding="UTF-8")
    pdf_file = HTML(string=rendered_html).write_pdf()
    http_response = HttpResponse(pdf_file, content_type='application/pdf')
    http_response['Content-Disposition'] = 'filename="constancia.pdf"'
    return http_response

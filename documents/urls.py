from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import DocumentListAPIView, CertificateViewSet, ConstacyCreateView, download_certificate, download_constancy

router = DefaultRouter()
router.register('certificates', CertificateViewSet, base_name='certificates')

urlpatterns = [
    path('documents/', DocumentListAPIView.as_view(), name='documents_list'),
    path('certificates/download/<int:pk>/', download_certificate, name='download_certificate'),
    path('constancies/create/', ConstacyCreateView.as_view()),
    path('constancies/download/<int:pk>/', download_constancy, name='download_constancy')
]

urlpatterns += router.urls

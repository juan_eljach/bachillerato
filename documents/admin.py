from django.contrib import admin
from .models import Document, Certificate, ConstancyTemplate, Constancy

class ConstancyAdmin(admin.ModelAdmin):
    list_display = ['student_name', 'identity', 'reference_code', 'timestamp']
    exclude = ('reference_code',)

admin.site.register(Document)
admin.site.register(Certificate)
admin.site.register(Constancy, ConstancyAdmin)
admin.site.register(ConstancyTemplate)

# Register your models here.

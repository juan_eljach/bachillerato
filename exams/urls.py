from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import ExamRetrieveView, CheckAnswerView, SittingViewSet

urlpatterns = [
    path('exams/<int:pk>/', ExamRetrieveView.as_view(), name='exam-detail-view'),
    path('exams/<int:pk>/sitting/', SittingViewSet.as_view({'post':'create', 'get':'retrieve', 'put':'update'})),
    path('exams/<int:pk>/checkanswer/', CheckAnswerView.as_view()),
]

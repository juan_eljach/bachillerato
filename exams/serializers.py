from rest_framework import serializers
from .models import Exam, MultipleChoiceQuestion, Answer, Sitting

class AnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = ('id', 'content',)

class MultipleChoiceQuestionSerializer(serializers.ModelSerializer):
    answers = AnswerSerializer(many=True, read_only=True)
    class Meta:
        model = MultipleChoiceQuestion
        fields = '__all__'

class SittingDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sitting
        fields = '__all__'

class SittingUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sitting
        fields = (
            'start_time',
            'end_time',
            'completed',
        )

class SittingCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sitting
        fields = (
            'user_profile',
            'exam',
            'start_time',
        )

class SittingReportSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sitting
        fields = (
            'exam_approved',
            'completed',
            'grade'
        )

class ExamDetailSerializer(serializers.ModelSerializer):
    questions = MultipleChoiceQuestionSerializer(many=True, read_only=True)
    sitting = SittingDetailSerializer(read_only=True)
    class Meta:
        model = Exam
        fields = '__all__'

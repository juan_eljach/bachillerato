from django.contrib import admin
from .models import MultipleChoiceQuestion, Answer, Exam, Sitting

class AnswerInline(admin.TabularInline):
    model = Answer

class MultipleChoiceQuestionAdmin(admin.ModelAdmin):
    inlines = [AnswerInline]

class ExamAdmin(admin.ModelAdmin):
    exclude = ('slug',)
    filter_horizontal = ('questions',)
    list_display = ['title', 'subject', 'module',]

class SittingAdmin(admin.ModelAdmin):
    readonly_fields = (
        'user_profile',
        'exam',
        'right_answered_questions',
        'wrong_answered_questions',
        'answered_questions',
        'grade',
        'completed',
        'exam_approved',
    )
    list_display = ['__str__', 'completed', 'exam_approved', 'grade', 'return_subject']

admin.site.register(MultipleChoiceQuestion, MultipleChoiceQuestionAdmin)
admin.site.register(Exam, ExamAdmin)
admin.site.register(Sitting, SittingAdmin)
# Register your models here.

import datetime
from django.conf import settings
from django.shortcuts import render, get_object_or_404
from rest_framework import serializers, viewsets, generics, exceptions
from rest_framework.exceptions import PermissionDenied
from rest_framework.response import Response
from rest_framework.views import APIView
from courses.models import Enrollment
from courses.permissions import IsEnrolledOnSubject, CanAccessModule
from .models import Exam, MultipleChoiceQuestion, Answer, Sitting
from .serializers import ExamDetailSerializer, SittingDetailSerializer, SittingCreateSerializer, SittingUpdateSerializer

class SittingViewSet(viewsets.ModelViewSet):
    queryset = Sitting.objects.all()
    serializer_class = SittingDetailSerializer

    def get_object(self, *args, **kwargs):
        user = self.request.user
        exam_pk = self.kwargs.get('pk')
        exam = get_object_or_404(Exam, pk=exam_pk)
        try:
            sitting = Sitting.objects.get(exam=exam, user_profile=user.studentprofile)
        except Sitting.DoesNotExist:
            sitting = None
        return sitting

    def get_serializer_class(self, *args, **kwargs):
        if self.action == 'retrieve':
            return SittingDetailSerializer
        elif self.action == 'create':
            return SittingCreateSerializer
        elif self.action == 'update' or self.action == 'partial_update':
            return SittingUpdateSerializer
        return self.serializer_class


class ExamRetrieveView(generics.RetrieveAPIView):
    queryset = Exam.objects.all()
    serializer_class = ExamDetailSerializer
#    permission_classes = (IsStudent,)

    def get_object(self):
        obj = super(ExamRetrieveView, self).get_object()
        subject_permission = IsEnrolledOnSubject()
        module_permission = CanAccessModule()
        if obj.subject:
            if not subject_permission.has_object_permission(self.request, subject_permission, obj.subject):
                raise PermissionDenied
        elif obj.module:
            if not module_permission.has_object_permission(self.request, module_permission, obj.module):
                raise PermissionDenied
        else:
            raise serializers.ValidationError('The exam has not been assigned to a Subject or a Module.')
        return obj

#        self.check_permissions
class CheckAnswerView(APIView):
    def post(self, request, *args, **kwargs):
        user = request.user
        data = {}
        if hasattr(user, 'studentprofile'):
            if 'answer' in request.data:
                answer_pk = request.data.get('answer')
                question_pk = request.data.get('question')
                exam_pk = self.kwargs.get('pk')
                exam = get_object_or_404(Exam, pk=exam_pk)
                question = get_object_or_404(MultipleChoiceQuestion, pk=question_pk)
                answer = get_object_or_404(Answer, pk=answer_pk)
                try:
                    sitting = Sitting.objects.get(exam=exam, user_profile=user.studentprofile)
                except Sitting.DoesNotExist:
                    sitting = Sitting.objects.create(exam=exam, user_profile=user.studentprofile, start_time=datetime.datetime.now())
                if sitting.completed:
                    raise serializers.ValidationError('The user already finished answering this exam.')
                if question in exam.questions.all():
                    if answer.question == question:
                        if exam.questions.all().count() == sitting.answered_questions.all().count():
                            raise serializers.ValidationError('The user already answered all the questions.')

                        if not question in sitting.answered_questions.all():
                            data['is_correct'] = answer.correct
                            if answer.correct:
                                sitting.right_answered_questions.add(question)
                            else:
                                sitting.wrong_answered_questions.add(question)
                            sitting.answered_questions.add(question)
                            if exam.questions.all().count() == sitting.answered_questions.all().count():
                                sitting.completed = True
                                sitting.grade = round(float(float(5.0 / sitting.answered_questions.all().count()) * sitting.right_answered_questions.all().count()), 1)
                                if sitting.grade >= settings.PASSING_MARK:
                                    sitting.exam_approved = True
                                else:
                                    sitting.exam_approved = False
                                sitting.save()
                                data['completed'] = sitting.completed
                                data['approved'] = sitting.exam_approved
                                data['grade'] = sitting.grade
                                subject_exists = sitting.exam.subject
                                if subject_exists:
                                    enrollment = Enrollment.objects.get(subject=subject_exists, user_profile=user.studentprofile)
                                    enrollment.subject_approved = sitting.exam_approved
                                    enrollment.save()
                            return Response(data)
                        else:
                            raise serializers.ValidationError('The user already answered this question.')
                    else:
                        raise serializers.ValidationError('The answer does not belong to the question.')
                else:
                    raise serializers.ValidationError('The questions does not belong to the exam.')
            else:
                raise serializers.ValidationError('Did not received the answer attribute')
        else:
            raise exceptions.PermissionDenied

# Create your views here.

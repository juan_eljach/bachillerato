from hashlib import sha1
from django.core.validators import MaxValueValidator
from django.db import models
from django.utils.translation import ugettext as _
from django.template import defaultfilters
from accounts.models import StudentProfile
from courses.models import Subject, Module

class MultipleChoiceQuestion(models.Model):
    content = models.TextField()
    explanation = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.content[:50]

class Answer(models.Model):
    question = models.ForeignKey(MultipleChoiceQuestion, on_delete=models.CASCADE, blank=False, null=False, related_name='answers')
    content = models.TextField()
    correct = models.BooleanField(blank=False, default=False)

class Exam(models.Model):
    title = models.CharField(max_length=50)
    description = models.TextField()
    module = models.OneToOneField(Module, on_delete=models.CASCADE, blank=True, null=True, related_name='exam')
    subject = models.OneToOneField(Subject, on_delete=models.CASCADE, blank=True, null=True, related_name='subject_exam')
    questions = models.ManyToManyField(MultipleChoiceQuestion)
    pass_mark = models.SmallIntegerField(blank=True, default=0, help_text=_('Percentage required to pass exam.'), validators=[MaxValueValidator(100)])
    limited_time = models.BooleanField(default=False)
    time_limit = models.SmallIntegerField(blank=True, null=True, help_text=_('Max. amount of time in minutes'))
    slug = models.SlugField()

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = defaultfilters.slugify(self.title)
            while True:
                try:
                    exam_exists = Exam.objects.get(slug__iexact=self.slug)
                    if exam_exists:
                        self.slug = "{0}-{1}".format(self.slug, sha1(str(random.random()).encode('utf-8')).hexdigest()[:5])
                except Exam.DoesNotExist:
                    break
        super(Exam, self).save(*args, **kwargs)

class Sitting(models.Model):
    user_profile = models.ForeignKey(StudentProfile, on_delete=models.CASCADE, blank=False, null=False)
    exam = models.OneToOneField(Exam, on_delete=models.CASCADE, blank=False, null=False, related_name='sitting')
    start_time = models.DateTimeField(blank=True, null=True)
    end_time = models.DateTimeField(blank=True, null=True)
    right_answered_questions = models.ManyToManyField(MultipleChoiceQuestion, related_name='right_questions')
    wrong_answered_questions = models.ManyToManyField(MultipleChoiceQuestion, related_name='wrong_questions')
    answered_questions = models.ManyToManyField(MultipleChoiceQuestion, related_name='answered_questions')
    exam_approved = models.BooleanField(default=False)
    completed = models.BooleanField(default=False)
    grade = models.FloatField(blank=True, null=True)

    def __str__(self):
        return ('Estudiante: {0} - Examen: {1}'.format(self.user_profile.user.get_full_name(), self.exam))

    def get_grade(self):
        if not self.exam.questions.all().count() == sitting.answered_questions.all().count():
            return ('Error - The user has not completed the exam')
        right = self.right_answered_questions.all().count()
        wrong = self.right_answered_questions.all().count()
        total = self.answered_questions.all().count()
        point_per_question = float(5.0/total)
        grade = float(right * point_per_question)
        return grade

    def return_subject(self):
        if self.exam.subject:
            return self.exam.subject
        else:
            return None

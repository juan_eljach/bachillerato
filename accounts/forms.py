import datetime
from django import forms
from dateutil.relativedelta import relativedelta
from bachillerato.choices import LAST_APPROVED_GRADE_CHOICES, ORDERED_COURSES_FOR_UNDERAGE, LAST_APPROVED_FOR_BASIC, LAST_APPROVED_FOR_MIDDLE
from courses.models import Course
from .models import StudentProfile, IDENTITY_TYPE_CHOICES

class CustomForm(forms.Form):
    """
    Custom Signup Form for django-allauth signup handling
    Declared at settings as ACCOUNT_SIGNUP_FORM_CLASS
    """
    first_name = forms.CharField(max_length=30)
    last_name = forms.CharField(max_length=30)
    cellphone = forms.CharField(max_length=30, required=False)
    identity = forms.CharField(max_length=30, required=False)
    identity_type = forms.ChoiceField(choices=IDENTITY_TYPE_CHOICES)
    last_approved_grade = forms.ChoiceField(choices=LAST_APPROVED_GRADE_CHOICES)
    birthdate = forms.DateField()

    def get_course_or_none(self, classmodel, **kwargs):
        try:
            return classmodel.objects.get(**kwargs)
        except classmodel.DoesNotExist:
            return None

    def signup(self, request, user):
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.save()
        profile = StudentProfile.objects.create(user=user)
        birthdate = self.cleaned_data['birthdate']
        last_approved_grade = self.cleaned_data['last_approved_grade']
        user.studentprofile.birthdate = birthdate
        user.studentprofile.cellphone = self.cleaned_data['cellphone']
        user.studentprofile.identity = self.cleaned_data['identity']
        user.studentprofile.identity_type = self.cleaned_data['identity_type']
        user.studentprofile.last_approved_grade = last_approved_grade
        user.studentprofile.save()
        today = datetime.date.today()
        difference = relativedelta(today, birthdate)
        if difference.years < 18:
            user.studentprofile.underage = True
            if last_approved_grade in ORDERED_COURSES_FOR_UNDERAGE:
                last_index = ORDERED_COURSES_FOR_UNDERAGE.index(last_approved_grade)
                current_grade = ORDERED_COURSES_FOR_UNDERAGE[last_index+1]
                user.studentprofile.current_grade =  self.get_course_or_none(Course, name=current_grade)
            else:
                user.studentprofile.current_grade =  self.get_course_or_none(Course, name='sixth')
        elif difference.years >= 18:
            if last_approved_grade in LAST_APPROVED_FOR_BASIC:
                user.studentprofile.current_grade =  self.get_course_or_none(Course, name='basic')
            elif last_approved_grade in LAST_APPROVED_FOR_MIDDLE:
                user.studentprofile.current_grade = self.get_course_or_none(Course, name='middle')
            else:
                pass
        user.studentprofile.save()

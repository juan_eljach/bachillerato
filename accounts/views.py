from django.http import HttpResponse
from django.shortcuts import redirect, get_object_or_404
from django.utils.decorators import method_decorator
from django.views.generic import RedirectView, View
from rest_framework import generics, permissions
from courses.models import Course
from .decorators import superuser_only
from .models import StudentProfile
from .serializers import StudentProfileSerializer, SmallStudentProfileSerializer

class CheckProfileView(RedirectView):
    def get_redirect_url(self, **kwargs):
        user = self.request.user
        if hasattr(user, 'studentprofile'):
            return "/app"
        else:
            return "/notieneperfil"


class UserProfileDetailView(generics.RetrieveUpdateAPIView):
    model_class = StudentProfile
    serializer_class = StudentProfileSerializer

    def get_object(self):
        profile = get_object_or_404(self.model_class, user=self.request.user)
        return profile

class UserListView(generics.ListAPIView):
    queryset = StudentProfile.objects.all()
    model_class = StudentProfile
    serializer_class = SmallStudentProfileSerializer

    def get_queryset(self):
        course_id = self.kwargs.get('id')
        course = get_object_or_404(Course, id=course_id)
        queryset = self.queryset
        return queryset.filter(current_grade=course)

@method_decorator(superuser_only, name='dispatch')
class UserExportView(View):
    def get(self, request, *args, **kwargs):
        default_col_initial = "\"" + "02" + "\""
        default_value = "0"
        default_date = "2016-03-30 08:00:00"
        default_cellphone = "\"" + "NA" + "\""
        default_code = "\"" + "" + "\""
        queryset = StudentProfile.objects.all()
        export_data = "\"01\"|REFERENCIA|VALOR|FECHA|NOMBRES|APELLIDO1|APELLIDO2|CELULAR|CODIGO|ALUMNO\n"
        for student in queryset:
            reference = "\"" + str(student.code) + "\""
            names = "\"" + str(student.user.first_name) + "\""
            last_names = "\"" + str(student.user.last_name) + "\""
            alumn = "\"" + str(student.code) + "\""
            data_array = [
                default_col_initial,
                reference,
                default_value,
                default_date,
                names,
                last_names,
                default_cellphone,
                default_code,
                alumn
            ]
            export_data += "|".join(data_array)
            export_data += "\n"
        end_line = "\"03\"|" + str(queryset.count()) + "|0|2016-03-30 08:00:00|||||"
        export_data += end_line
        response = HttpResponse(export_data, content_type='text/plain; charset=utf8')
        response['Content-Disposition'] = 'filename="efecty-export.pdf"'
        return response

from django.contrib.auth import get_user_model
from rest_framework import serializers
from courses.serializers import EnrollmentSerializer
from .models import StudentProfile
user_model = get_user_model()

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = user_model
        fields = ('id', 'first_name', 'last_name', 'email')

class StudentProfileSerializer(serializers.ModelSerializer):
    enrollments = EnrollmentSerializer(many=True, read_only=True)
    user = UserSerializer(read_only=True)
    class Meta:
        model = StudentProfile
        fields = '__all__'
        read_only_fields = ('user', 'code', 'last_approved_grade', 'semester', 'underage', 'timestamp', 'current_grade')

class SmallStudentProfileSerializer(serializers.ModelSerializer):
    full_name = serializers.SerializerMethodField()
    class Meta:
        model = StudentProfile
        fields = ('pk', 'full_name')

    def get_full_name(self, obj):
        return obj.user.get_full_name()

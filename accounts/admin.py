from django import forms
from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from searchableselect.widgets import SearchableSelect
from .models import CustomUser, StudentProfile, TeacherProfile, Parent
from .resources import StudentProfileResource

class StudentProfileAdmin(admin.ModelAdmin):
    search_fields = ['user__email', 'user__first_name', 'user__last_name' ,'cellphone']
    list_display = ['__str__', 'get_email' ,'cellphone', 'current_grade', 'semester']
    list_filter = ('current_grade',)

    def get_email(self, obj):
        return obj.user.email
    get_email.short_description = 'Email'


class StudentResourceAdmin(ImportExportModelAdmin):
    search_fields = ['user__email', 'user__first_name', 'user__last_name' ,'identity']
    list_display = ['__str__', 'identity', 'identity_type', 'last_approved_grade', 'current_grade', 'underage']
    resource_class = StudentProfileResource


class ParentForm(forms.ModelForm):
    class Meta:
        model = Parent
        exclude = ()
        widgets = {
            'student': SearchableSelect(model='accounts.StudentProfile', search_field='identity', many=False, limit=1)
        }

class ParentAdmin(admin.ModelAdmin):
    form = ParentForm




admin.site.register(CustomUser)
admin.site.register(StudentProfile, StudentResourceAdmin)
admin.site.register(TeacherProfile)
admin.site.register(Parent, ParentAdmin)

# Register your models here.

from django.contrib.auth.models import AbstractUser
from django.contrib.sites.models import Site
from django.contrib.sites.shortcuts import get_current_site
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models import F
from django.utils.translation import gettext_lazy as _
from bachillerato.choices import LEVEL_OF_STUDIES_CHOICES, CIVIL_STATUS_CHOICES, STUDENT_STATUS, SCHOOLS_ON_ALLIANCE, GRADE_CHOICES, STUDENT_DOCUMENT_CHOICES, SEMESTER_CHOICES

IDENTITY_CARD = "TI"
CITIZENSHIP_CARD = "CC"

IDENTITY_TYPE_CHOICES = (
    (IDENTITY_CARD, _("Identity Card")),
    (CITIZENSHIP_CARD, _("Citizenship Card"))
)

class CustomUser(AbstractUser):
    cellphone = models.CharField(max_length=20, blank=True, null=True)
    city = models.CharField(max_length=50, blank=True, null=True)

class CodeCounter(models.Model):
    counter = models.IntegerField(default=140000)
    site = models.OneToOneField(Site, on_delete=models.CASCADE)

class StudentProfile(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE, primary_key=True)
    code = models.IntegerField(blank=True, null=True)
    image = models.ImageField(upload_to='uploads', blank=True, null=True)
    identity = models.CharField(max_length=20, blank=True, null=True)
    identity_type = models.CharField(max_length=5, choices=IDENTITY_TYPE_CHOICES, blank=True, null=True)
    expedition_place = models.CharField(max_length=50, blank=True, null=True)
    birthdate = models.DateField(blank=True, null=True)
    civil_status = models.CharField(max_length=10, choices=CIVIL_STATUS_CHOICES, blank=True, null=True)
    cellphone = models.CharField(max_length=20, blank=True, null=True)
    address = models.CharField(max_length=40, blank=True, null=True)
    city = models.CharField(max_length=40, blank=True, null=True)
    neighborhood = models.CharField(max_length=40, blank=True, null=True)
    eps = models.CharField(max_length=30, blank=True, null=True)
    status = models.CharField(max_length=20, choices=STUDENT_STATUS, blank=True, null=True)
    school_alliance = models.CharField(max_length=20, choices=SCHOOLS_ON_ALLIANCE, blank=True, null=True)
    last_approved_grade = models.CharField(max_length=20, choices=GRADE_CHOICES, blank=True, null=True)
    current_grade = models.ForeignKey('courses.Course', on_delete=models.SET_NULL, blank=True, null=True)
    socioeconomic_status = models.IntegerField(choices=list(zip(range(1, 6), range(1, 6))), blank=True, null=True)
    sisben = models.IntegerField(choices=list(zip(range(1, 6), range(1, 6))), blank=True, null=True)
    semester = models.CharField(max_length=7, choices=SEMESTER_CHOICES, blank=True, null=True)
    underage = models.BooleanField(default=False)
    timestamp = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.user.get_full_name()

    def save(self, *args, **kwargs):
        site = Site.objects.first()
        if not self.code:
            code_counter, created = CodeCounter.objects.get_or_create(site=site)
            if not created:
                code_counter.counter = F('counter') + 1
                code_counter.save()
                counter = CodeCounter.objects.get(pk=code_counter.pk).counter
            else:
                counter = code_counter.counter
            self.code = counter
        super(StudentProfile, self).save(*args, **kwargs)

class TeacherProfile(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE, primary_key=True)
    education_level = models.CharField(max_length=20, choices=LEVEL_OF_STUDIES_CHOICES, blank=False)
    years_of_experience = models.PositiveIntegerField()
    college_degree = models.CharField(max_length=50)

    def __str__(self):
        return self.user.get_full_name()

class HomeInfo(models.Model):
    user_profile = models.OneToOneField(StudentProfile, on_delete=models.CASCADE, unique=True, related_name='home_info')
    country = models.CharField(max_length=30)
    state = models.CharField(max_length=40)
    city = models.CharField(max_length=40)
    neighborhood = models.CharField(max_length=40)
    address = models.CharField(max_length=40)
    home_phone = models.CharField(max_length=15)
    cellphone = models.CharField(max_length=15)

class DocumentDelivery(models.Model):
    user_profile = models.OneToOneField(StudentProfile, on_delete=models.CASCADE, unique=True, related_name='document_delivery')
    country = models.CharField(max_length=30)
    state = models.CharField(max_length=40)
    city = models.CharField(max_length=40)
    neighborhood = models.CharField(max_length=40)
    address = models.CharField(max_length=40)
    home_phone = models.CharField(max_length=15)
    cellphone = models.CharField(max_length=15)

class Parent(models.Model):
    student = models.ForeignKey(StudentProfile, on_delete=models.CASCADE, related_name='parents')
    name = models.CharField(max_length=100)
    home_phone = models.CharField(max_length=15)
    cellphone = models.CharField(max_length=15)
    email = models.EmailField()
    address = models.CharField(max_length=100)
    neighborhood = models.CharField(max_length=30)
    city = models.CharField(max_length=30)
    state = models.CharField(max_length=30)
    relationship = models.CharField(max_length=40)

    def __str__(self):
        return "Parent of: {}".format(self.student.user.get_full_name())

"""
@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
"""

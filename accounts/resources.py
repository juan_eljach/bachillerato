from import_export import resources
from .models import StudentProfile

class StudentProfileResource(resources.ModelResource):
    class Meta:
        model = StudentProfile
        fields = (
            'user__first_name',
            'user__last_name',
            'identity',
            'identity_type',
            'expedition_place',
            'cellphone',
            'home_info__city',
            'home_info__address',
            'home_info__neighborhood',
            'home_info__home_phone',
        )
        export_order = fields

from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import CheckProfileView, UserProfileDetailView, UserListView, UserExportView

urlpatterns = [
    path('export/', UserExportView.as_view(), name='user-export'),
    path('checkprofile/', CheckProfileView.as_view(), name='check-profile'),
    path('userprofile/', UserProfileDetailView.as_view(), name='user-profile'),
    path('userlist/<int:id>/', UserListView.as_view(), name='user-profile'),
]

from django.contrib import admin
from .models import Dictation, DictationSitting, RelationshipQuestion, Relationship, RelationshipQuestionSitting, MultipleChoiceQuestion, Answer, MultipleChoiceQuestionSitting, AreaActivity, AreaAnswer

class RelationshipInline(admin.TabularInline):
	model = Relationship

class RelationshipQuestionAdmin(admin.ModelAdmin):
	inlines = [RelationshipInline]

class AnswerInline(admin.TabularInline):
	model = Answer

class MultipleChoiceQuestionAdmin(admin.ModelAdmin):
	inlines = [AnswerInline]

class DictationSittingAdmin(admin.ModelAdmin):
	list_display = ['dictation', 'student', 'number_of_errors', 'grade']

class AreaAnswerInline(admin.TabularInline):
	model = AreaAnswer

class AreaActivityAdmin(admin.ModelAdmin):
	inlines = [AreaAnswerInline]

admin.site.register(Dictation)
admin.site.register(RelationshipQuestion, RelationshipQuestionAdmin)
admin.site.register(MultipleChoiceQuestion, MultipleChoiceQuestionAdmin)
admin.site.register(MultipleChoiceQuestionSitting)
admin.site.register(DictationSitting, DictationSittingAdmin)
admin.site.register(RelationshipQuestionSitting)
admin.site.register(AreaActivity, AreaActivityAdmin)

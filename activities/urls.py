from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import MCQuestionSittingCreateView, DictationSittingCreateView, RelationshipQuestionSittingCreateView, FullDisplayDictationDetailView, AreaActivitySittingCreateView

urlpatterns = [
    path('mcquestion/checkanswer/', MCQuestionSittingCreateView.as_view()),
    path('dictation/<int:dictation_pk>/full-display/', FullDisplayDictationDetailView.as_view()),
    path('dictation/checkanswer/', DictationSittingCreateView.as_view()),
    path('relationship/checkanswer/', RelationshipQuestionSittingCreateView.as_view()),
    path('area/checkanswer/', AreaActivitySittingCreateView.as_view()),
]

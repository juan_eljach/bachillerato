from django.conf import settings
from django.shortcuts import render
from rest_framework import generics, exceptions, serializers
from rest_framework.response import Response
from rest_framework.views import APIView
from courses.permissions import CanAccessModule
from .models import MultipleChoiceQuestion, Answer, RelationshipQuestionSitting, Dictation, AreaActivitySitting
from .serializers import MCQuestionSittingSerializer, DictationSittingSerializer, FullDisplayDictationSerializer, RelationshipQuestionSittingSerializer, AreaActivitySittingSerializer

class MCQuestionSittingCreateView(generics.CreateAPIView):
    serializer_class = MCQuestionSittingSerializer

    def create(self, request, *args, **kwargs):
        user = request.user
        if hasattr(user, 'studentprofile'):
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            question = serializer.validated_data.get('mc_question')
            if hasattr(question, 'sitting'):
                raise serializers.ValidationError('The user already answered this question')
            else:
                pass
            given_answer = serializer.validated_data.get('given_answer')
            module_permission = CanAccessModule()
            module = question.module
            can_answer_question = module_permission.has_object_permission(request, module_permission, module)
            if can_answer_question:
                if given_answer.question == question:
                    print ("Given answer is correct? {}".format(given_answer.correct))
                    is_right_answer = given_answer.correct
                    if is_right_answer:
                        grade = settings.MAX_GRADE
                    else:
                        grade = settings.MIN_GRADE
                    self.perform_create(serializer, student=user.studentprofile, is_right_answer=is_right_answer, grade=grade)
                    return Response(serializer.data)
            else:
                raise exceptions.PermissionDenied
        else:
            raise serializers.ValidationError('The user has not profile')
            #self.perform_create(serializer)

    def perform_create(self, serializer, *args, **kwargs):
        serializer.save(**kwargs)

class DictationSittingCreateView(generics.CreateAPIView):
    serializer_class = DictationSittingSerializer

    def create(self, request, *args, **kwargs):
        user = request.user
        if hasattr(user, 'studentprofile'):
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            dictation = serializer.validated_data.get('dictation')
            if hasattr(dictation, 'sitting'):
                raise serializers.ValidationError('The user already answered this question')
            else:
                pass
            module_permission = CanAccessModule()
            module = dictation.module
            can_answer_question = module_permission.has_object_permission(request, module_permission, module)
            if can_answer_question:
                    self.perform_create(serializer, student=user.studentprofile)
                    return Response(serializer.data)
            else:
                raise exceptions.PermissionDenied
        else:
            raise serializers.ValidationError('The user has not profile')
            #self.perform_create(serializer)

    def perform_create(self, serializer, *args, **kwargs):
        serializer.save(**kwargs)

class RelationshipQuestionSittingCreateView(generics.CreateAPIView):
    queryset = RelationshipQuestionSitting.objects.all()
    serializer_class = RelationshipQuestionSittingSerializer

    def create(self, request, *args, **kwargs):
        user = request.user
        if hasattr(user, 'studentprofile'):
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            relationship_question = serializer.validated_data.get('relationship_question')
            if hasattr(relationship_question, 'sitting'):
                raise serializers.ValidationError('The user already answered this question')
            else:
                pass
            module_permission = CanAccessModule()
            module = relationship_question.module
            can_answer_question = module_permission.has_object_permission(request, module_permission, module)
            if can_answer_question:
                    self.perform_create(serializer, student=user.studentprofile)
                    return Response(serializer.data)
            else:
                raise exceptions.PermissionDenied
        else:
            raise serializers.ValidationError('The user has not profile')
            #self.perform_create(serializer)

    def perform_create(self, serializer, *args, **kwargs):
        serializer.save(**kwargs)

class AreaActivitySittingCreateView(generics.CreateAPIView):
    queryset = AreaActivitySitting.objects.all()
    serializer_class = AreaActivitySittingSerializer

    def create(self, request, *args, **kwargs):
        user = request.user
        if hasattr(user, 'studentprofile'):
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            area_activity = serializer.validated_data.get('area_activity')
            if hasattr(area_activity, 'sitting'):
                raise serializers.ValidationError('The user already answered this question')
            else:
                pass
            module_permission = CanAccessModule()
            module = area_activity.module
            can_answer_question = module_permission.has_object_permission(request, module_permission, module)
            if can_answer_question:
                number_of_errors = float(serializer.validated_data.get('number_of_errors'))
                total_number_of_questions = float(area_activity.answers.all().count())
                right_answered = float(total_number_of_questions - number_of_errors)
                grade = (right_answered * 5.0) / total_number_of_questions
                self.perform_create(serializer, student=user.studentprofile, grade=grade)
                return Response(serializer.data)
            else:
                raise exceptions.PermissionDenied
        else:
            raise serializers.ValidationError('The user has not profile')
            #self.perform_create(serializer)

    def perform_create(self, serializer, *args, **kwargs):
        serializer.save(**kwargs)


class FullDisplayDictationDetailView(generics.RetrieveAPIView):
    queryset = Dictation.objects.all()
    model_class = Dictation
    serializer_class = FullDisplayDictationSerializer
    lookup_field = 'pk'
    lookup_url_kwarg = 'dictation_pk'

"""
class CheckMCAnswerView(APIView):
    def post(self, request, *args, **kwargs):
        if 'question' in request.data and 'answer' in request.data:
            user = request.user
            if hasattr(user, 'studentprofile'):
                serializer = MCQuestionAnswerSerializer(request.data)
                serializer.is_valid()
                question_id = serializer.validated_data.get(question)
                answer_id = serializer.validated_data.get(answer)
"""

#class CheckDictationAnswerView()


#class CheckAreasAnswerView()


#class CheckRelationshipsAnswerView()

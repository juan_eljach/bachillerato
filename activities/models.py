from django.db import models
from accounts.models import StudentProfile
from courses.models import Subject, Module

class AreaActivity(models.Model):
    module = models.ForeignKey(Module, on_delete=models.CASCADE, blank=False, null=False, related_name='areas')
    title = models.CharField(max_length=50, blank=True, null=True)
    image = models.ImageField(upload_to='activities', blank=False, null=False)
    image_height = models.IntegerField()
    image_width = models.IntegerField()

class AreaAnswer(models.Model):
    area_activity = models.ForeignKey(AreaActivity, on_delete=models.CASCADE, blank=False, null=False, related_name='answers')
    explainatory_text = models.CharField(max_length=100, blank=False, null=False)
    coordinates = models.CharField(max_length=100, blank=False, null=False)

class AreaActivitySitting(models.Model):
    student = models.ForeignKey(StudentProfile, on_delete=models.CASCADE, blank=False, null=False, related_name='areas_sittings')
    area_activity = models.OneToOneField(AreaActivity, on_delete=models.CASCADE, related_name="sitting")
    number_of_errors = models.IntegerField(default=0)
    timestamp = models.DateTimeField(auto_now_add=True)

class Dictation(models.Model):
    module = models.ForeignKey(Module, on_delete=models.CASCADE, blank=False, null=False, related_name='dictations')
    title = models.CharField(max_length=50, blank=True, null=True)
    audio = models.FileField(upload_to='audios')
    audio_to_text = models.TextField()

    def __str__(self):
        return ("Dictation in Module: {}".format(self.module))

class DictationSitting(models.Model):
    student = models.ForeignKey(StudentProfile, on_delete=models.CASCADE, blank=False, null=False, related_name='dictations_sittings')
    dictation = models.OneToOneField(Dictation, on_delete=models.CASCADE, related_name="sitting")
    grade = models.FloatField(blank=True, null=True)
    number_of_errors = models.IntegerField(default=0)
    answer = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)

class RelationshipQuestion(models.Model):
    module = models.ForeignKey(Module, on_delete=models.CASCADE, blank=False, null=False, related_name='relationships')
    title = models.CharField(max_length=50, blank=True, null=True)
    content = models.TextField()

    def __str__(self):
        return ("{}...".format(self.content[:20]))

class Relationship(models.Model):
    question = models.ForeignKey(RelationshipQuestion, on_delete=models.CASCADE, blank=False, null=False, related_name='answers')
    column_a_content = models.CharField(max_length=30, blank=True, null=True)
    column_a_image = models.ImageField(upload_to="uploads/images", blank=True, null=True)
    column_b_content = models.CharField(max_length=30, blank=True, null=True)
    column_b_image = models.ImageField(upload_to="uploads/images", blank=True, null=True)

    def __str__(self):
        return ("Relationship answer (id: {})".format(self.pk))

class RelationshipQuestionSitting(models.Model):
    student = models.ForeignKey(StudentProfile, on_delete=models.CASCADE, blank=False, null=False, related_name='relationship_sittings')
    relationship_question = models.OneToOneField(RelationshipQuestion, on_delete=models.CASCADE, blank=False, null=False, related_name='sitting')
    grade = models.FloatField(blank=True, null=True)
    right_relationships = models.ManyToManyField(Relationship, blank=True, related_name='right_answered')
    wrong_relationships = models.ManyToManyField(Relationship, blank=True, related_name='wrong_answered')
    user_response = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)

#----------------------------------------------------------------------------
class MultipleChoiceQuestion(models.Model):
    module = models.ForeignKey(Module, on_delete=models.CASCADE, blank=False, null=False, related_name='mc_questions')
    title = models.CharField(max_length=50, blank=True, null=True)
    content = models.TextField()
    explanation = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.content[:50]

#Answer Model for MultipleChoiceQuestion
class Answer(models.Model):
    question = models.ForeignKey(MultipleChoiceQuestion, on_delete=models.CASCADE, blank=False, null=False, related_name='answers')
    content = models.TextField()
    correct = models.BooleanField(blank=False, default=False)

class MultipleChoiceQuestionSitting(models.Model):
    student = models.ForeignKey(StudentProfile, on_delete=models.CASCADE, blank=False, null=False, related_name='mc_questions_sittings')
    mc_question = models.OneToOneField(MultipleChoiceQuestion, on_delete=models.CASCADE, blank=False, null=False, related_name='sitting')
    given_answer = models.ForeignKey(Answer, on_delete=models.CASCADE, blank=True, null=True)
    is_right_answer = models.BooleanField(default=False)
    grade = models.FloatField(blank=True, null=True)
    timestamp = models.DateTimeField(auto_now_add=True)


#class ActivitySitting(models.Model):
#    user_profile = models.ForeignKey(StudentProfile, on_delete=models.CASCADE, blank=False, null=False)
#    activity = models.OneToOneField(Activity, on_delete=models.CASCADE, blank=False, null=False)


"""

HACER UN ENDPOINT DE CHECKANSWER PARA CADA TIPO DE ACTIVIDAD. HACER UN SITTING PARA CADA TIPO DE ACTIVIDAD
Y PEGARLO AL ACTIVITYSITTING. LUEGO SIMPLEMENTE CALCULAR EL GRADE DENTRO DEL ACTIVITYSITTING CON UN MODEL METHOD Y PEGARLO
AL SERIALIZER DEL MODULE"""

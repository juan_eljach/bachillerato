from rest_framework import serializers
from .models import (Dictation, DictationSitting, RelationshipQuestion, Relationship ,RelationshipQuestionSitting, MultipleChoiceQuestion,
    Answer, MultipleChoiceQuestionSitting, AreaActivity, AreaAnswer, AreaActivitySitting
)

class AreaAnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = AreaAnswer
        fields = '__all__'

class AreaActivitySerializer(serializers.ModelSerializer):
    answers = AreaAnswerSerializer(many=True, read_only=True)
    class Meta:
        model = AreaActivity
        fields = '__all__'

class AreaActivitySittingSerializer(serializers.ModelSerializer):
    class Meta:
        model = AreaActivitySitting
        fields = '__all__'
        read_only_fields = ('student', 'timestamp')

class DictationSittingSerializer(serializers.ModelSerializer):
    class Meta:
        model = DictationSitting
        fields = '__all__'
        read_only_fields = ('student', 'timestamp',)

class DictationSerializer(serializers.ModelSerializer):
    sitting = DictationSittingSerializer(read_only=True)
    class Meta:
        model = Dictation
#        fields = '__all__'
        exclude = ('audio_to_text',)

class FullDisplayDictationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dictation
        fields = '__all__'

class RelationshipQuestionSittingSerializer(serializers.ModelSerializer):
    class Meta:
        model = RelationshipQuestionSitting
        fields = '__all__'
        read_only_fields = ('student', 'grade', 'timestamp')

class RelationshipSerializer(serializers.ModelSerializer):
    class Meta:
        model = Relationship
        fields = '__all__'

class RelationshipQuestionSerializer(serializers.ModelSerializer):
    answers = RelationshipSerializer(many=True, read_only=True)
    sitting = RelationshipQuestionSittingSerializer(read_only=True)
    class Meta:
        model = RelationshipQuestion
        fields = '__all__'

class MCQuestionSittingSerializer(serializers.ModelSerializer):
    class Meta:
        model = MultipleChoiceQuestionSitting
        fields = '__all__'
        read_only_fields = ('student', 'is_right_answer', 'grade', 'timestamp')

class AnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = '__all__'

class MultipleChoiceQuestionSerializer(serializers.ModelSerializer):
    answers = AnswerSerializer(many=True, read_only=True)
    sitting = MCQuestionSittingSerializer()
    class Meta:
        model = MultipleChoiceQuestion
        fields = '__all__'

from rest_framework import permissions

class IsMemberOfChat(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        studentprofile = request.user.studentprofile
        return studentprofile in obj.members.all()

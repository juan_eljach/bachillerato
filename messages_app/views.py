from django.shortcuts import render, get_object_or_404
from rest_framework import viewsets, generics, exceptions
from rest_framework.response import Response
from .models import Chat, Message
from .permissions import IsMemberOfChat
from .serializers import ChatSerializer, MessageSerializer, MessageAndChatSerializer, MassiveMessageSerializer
from rest_framework.permissions import IsAdminUser

class ChatViewSet(viewsets.ModelViewSet):
    permission_classes = (IsMemberOfChat,)
    serializer_class = ChatSerializer

    def get_queryset(self):
        user = self.request.user
        chats = Chat.objects.filter(members=user.studentprofile)
        return chats

class MessageCreateView(generics.CreateAPIView):
    serializer_class = MessageSerializer

    def create(self, request, *args, **kwargs):
        user = request.user
        chat_pk = self.kwargs.get('chat_pk')
        chat = get_object_or_404(Chat, pk=chat_pk)
        serializer = self.get_serializer(data=request.data)
        serializer.sender = user.studentprofile
        serializer.is_valid(raise_exception=True)
        chat_permission = IsMemberOfChat()
        can_send_message = chat_permission.has_object_permission(request, chat_permission, chat)
        if can_send_message:
            self.perform_create(serializer, chat=chat)
            return Response(serializer.data)
        else:
            raise exceptions.PermissionDenied

    def perform_create(self, serializer, *args, **kwargs):
        chat = kwargs.get('chat')
        serializer.save(sender=self.request.user.studentprofile, chat=chat)

class MessageAndChatCreateView(generics.CreateAPIView):
    serializer_class = MessageAndChatSerializer

    def create(self, request, *args, **kwargs):
        user = request.user
        serializer = self.get_serializer(data=request.data)
        serializer.sender = user.studentprofile
        serializer.is_valid(raise_exception=True)
        members = serializer.validated_data.get('members')
        body = serializer.validated_data.get('body')
        chat_exist = Chat.objects.filter(members=members[0]).filter(members=members[1])
        if chat_exist and user.studentprofile.pk in members:
            print ("GOT INTO IF")
            chat = chat_exist[0]
            print ("Instantiated chat")
            chat_permission = IsMemberOfChat()
            can_send_message = chat_permission.has_object_permission(request, chat_permission, chat)
            if can_send_message:
                Message.objects.create(body=body, sender=user.studentprofile, chat=chat)
            else:
                raise exceptions.PermissionDenied
        elif user.studentprofile.pk in members:
            print ("GOT INTO ELSE")
            chat = Chat.objects.create()
            chat.members.set(members)
            chat.save()
            Message.objects.create(body=body, sender=user.studentprofile, chat=chat)
        return Response(serializer.data)

class MassiveMessageCreateView(generics.CreateAPIView):
    serializer_class = MassiveMessageSerializer
    permission_classes = [IsAdminUser]
    #add logic to send message to current_user of course

from django.db import models
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext as _
from courses.models import Course
from accounts.models import StudentProfile

class Chat(models.Model):
    members = models.ManyToManyField(StudentProfile)

    def __str__(self):
        members =  [m.user.get_full_name() if len(m.user.get_full_name()) > 0 else 'None' for m in self.members.all()]
        return ('Members: {}'.format(members))

class Message(models.Model):
    chat = models.ForeignKey(Chat, on_delete=models.CASCADE, blank=False, null=False, related_name='messages')
    body = models.TextField()
    sender = models.ForeignKey(StudentProfile, on_delete=models.CASCADE, blank=False, null=False, related_name='sent_messages')
    sent_at = models.DateTimeField(_("sent at"), null=True, blank=True)
    read = models.BooleanField(default=False)

class MassiveMessage(models.Model):
    course = models.ForeignKey(Course, on_delete=models.CASCADE, blank=False, null=False)
    text = models.TextField()
    subject = models.CharField(max_length=50, blank=True, null=True)
    sent = models.BooleanField(default=False)
    timestamp = models.DateTimeField(auto_now_add=True)

    def send_massive_message(self, *args, **kwargs):
        course = self.course
        content = self.text
        subject = self.subject
#		template_name = settings.QUOTATION_TEMPLATE_ID
        html_text = render_to_string("presales/emails/quotation_email_template.html")
        queryset = StudentProfile.objects.filter(current_grade=course)
        SendMassiveEmail.delay(
			course,
			content,
            queryset,
			from_email,
			from_name,
			subject,
			html_text,
			substitutions,
		)
        return True

    def save(self, *args, **kwargs):
        super(MassiveMessage, self).save(*args, **kwargs)
        if not self.sent:
            send_message = self.send_message()
            self.sent = send_message
            super(MassiveMessage, self).save(*args, **kwargs)

        #send email with async task

from rest_framework import serializers
from accounts.serializers import UserSerializer
from .models import Chat, Message, MassiveMessage

class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'
        read_only_fields = ('chat', 'sender', 'read')

class MessageAndChatSerializer(serializers.Serializer):
    members = serializers.ListField()
    body = serializers.CharField()

    def create(self, validated_data):
        return Message.objects.create(**validated_data)
#    def get_sender(self, obj):
#        return obj.sender.get_full_name()

class MassiveMessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = MassiveMessage
        fields = '__all__'
        read_only_fields = ('sent',)

class ChatSerializer(serializers.ModelSerializer):
    messages = MessageSerializer(many=True, read_only=True)
    class Meta:
        model = Chat
        fields = '__all__'

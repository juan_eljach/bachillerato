from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import ChatViewSet, MessageCreateView, MessageAndChatCreateView, MassiveMessageCreateView

router = DefaultRouter()
router.register('chats', ChatViewSet, base_name='chat')

urlpatterns = [
    path('chats/messages/send/', MessageAndChatCreateView.as_view()),
    path('chats/<int:chat_pk>/messages/send/', MessageCreateView.as_view()),
    path('chats/massive-message/', MassiveMessageCreateView.as_view()),
]

urlpatterns += router.urls

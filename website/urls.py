from django.urls import path
from allauth.account.views import SignupView
from .views import ContactTemplateView, FAQsTemplateView, AppTemplateView

urlpatterns = [
    path('', SignupView.as_view(), name='account_signup'),
    path('contacto/', ContactTemplateView.as_view()),
    path('faqs/', FAQsTemplateView.as_view()),
    path('app/', AppTemplateView.as_view()),
]

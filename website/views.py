from django.shortcuts import render
from django.views.generic import TemplateView

class LandingTemplateView(TemplateView):
	template_name = "home/landing.html"

class ContactTemplateView(TemplateView):
	template_name = "home/contact.html"

class FAQsTemplateView(TemplateView):
	template_name = "home/faqs.html"

class AppTemplateView(TemplateView):
	template_name = "app/index.html"

export const courses = [
  {label: 'Sexto', value: 'sixth'},
  { label: 'Séptimo', value: 'seventh' },
  { label: 'Octavo', value: 'eighth' },
  { label: 'Noveno', value: 'ninth' },
  { label: 'Décimo', value: 'tenth' },
  { label: 'Undécimo', value: 'eleven' },
  { label: 'Medio', value: 'middle' },
  { label: 'Básico', value: 'basic' },
];

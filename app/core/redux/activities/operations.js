import { xhrApi as xhr } from 'xhr';

const respondSelection = payload => async () => {
	const response = await xhr.post('/activities/mcquestion/checkanswer/', payload);

	return response.data;
};

const getFullDisplayDictation = id => async () => {
	const response = await xhr.get(`activities/dictation/${id}/full-display/`);

	return response.data;
};

const respondDictation = payload => async () => {
	const response = await xhr.post('/activities/dictation/checkanswer/', payload);

	return response.data;
};

const respondRelationship = payload => async () => {
	const response = await xhr.post('/activities/relationship/checkanswer/', payload);

	return response.data;
};

export { respondSelection, getFullDisplayDictation, respondDictation, respondRelationship };

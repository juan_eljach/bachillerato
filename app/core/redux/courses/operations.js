import { xhrApi as xhr } from 'xhr';

const getCourses = () => async () => {
  const response = await xhr.get('/courses');

  return response.data;
};

const getCourse = (slug) => async () => {
  const response = await xhr.get(`/courses/${slug}`);

  return response.data;
};

export {
  getCourses,
  getCourse
};

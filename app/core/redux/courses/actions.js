import * as types from './types';

const setCourse = payload => ({
	type: types.SET_COURSE,
	payload
});

export { setCourse };

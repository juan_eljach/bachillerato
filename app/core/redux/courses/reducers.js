import * as types from './types';

const coursesReducer = (state = { current: {} }, action) => {
	switch (action.type) {
		case types.SET_COURSE: {
			return { ...state, current: action.payload };
		}

		default:
			return state;
	}
};

export default coursesReducer;

import user from './user';
import { reducer as formReducer } from 'redux-form';
import courses from './courses';

const reducers = {
	user,
	courses,
	form: formReducer
};

const getReducers = reducersList => {
	const reducersMapped = reducersList.reduce((acc, current) => {
		return {
			...acc,
			[current]: reducers[current]
		};
	}, {});

	return reducersMapped;
};

export default getReducers;

import { xhrApi as xhr } from 'xhr';

const getExam = examId => async () => {
	const response = await xhr.get(`/exams/${examId}`);

	return response.data;
};

const checkAnswer = (examId, payload) => async () => {
	const response = await xhr.post(`/exams/${examId}/checkanswer/`, payload);

	return response.data;
};

export { getExam, checkAnswer };

import { xhrApi as xhr } from 'xhr';

const getTasks = courseSlug => async () => {
	const response = await xhr.get(`/questions/course/${courseSlug}/`);

	return response.data;
};

const sendTask = payload => async () => {
	const response = await xhr.post('/questions/checkanswer/', payload);

	return response.data;
};

export { getTasks, sendTask };

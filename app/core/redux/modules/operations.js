import { xhrApi as xhr } from 'xhr';

const getModules = (courseSlug, subjectSlug) => async () => {
  const response = await xhr.get(
    `/courses/${courseSlug}/subjects/${subjectSlug}/modules/`
  );

  return response.data;
};

const getModule = (courseSlug, subjectSlug, moduleSlug) => async () => {
  const response = await xhr.get(
    `/courses/${courseSlug}/subjects/${subjectSlug}/modules/${moduleSlug}/`
  );

  return response.data;
};

export {
  getModules,
  getModule
};

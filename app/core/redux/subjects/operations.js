import { xhrApi as xhr } from 'xhr';

const getSubjects = courseSlug => async () => {
	const response = await xhr.get(`/courses/${courseSlug}/subjects/`);

	return response.data;
};

const getSubject = (courseSlug, subjectSlug) => async () => {
	const response = await xhr.get(`/courses/${courseSlug}/subjects/${subjectSlug}`);

	return response.data;
};

export { getSubjects, getSubject };

import xhr, { xhrApi } from 'xhr';

const getChats = () => async () => {
	const response = await xhrApi.get('/chats/');

	return response.data;
};

const getChatDetail = chatId => async () => {
	const response = await xhrApi.get(`/chats/${chatId}/`);

	return response.data;
};

const getCourseUsers = courseId => async () => {
	const response = await xhr.get(`/userlist/${courseId}/`);

	return response.data;
};

const sendMessage = payload => async () => {
	const response = await xhrApi.post('/chats/messages/send/', payload);

	return response.data;
};

export { getChats, getChatDetail, getCourseUsers, sendMessage };

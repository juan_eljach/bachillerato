import xhr, { xhrApi } from 'xhr';

import * as actions from './actions';

const getUser = () => async dispatch => {
	const { data } = await xhr.get('/userprofile');

	const { user, ...rest } = data;
	const userInfo = { ...user, ...rest };

	dispatch(actions.setUser(userInfo));

	return userInfo;
};

const updateProfile = payload => async dispatch => {
	const { data } = await xhr.put('/userprofile/', payload);

	const { user, ...rest } = data;
	const userInfo = { ...user, ...rest };

	dispatch(actions.setUser(userInfo));

	return userInfo;
};

const getDocuments = () => async () => {
	const { data } = await xhrApi.get('/documents/');

	return data;
};

const getStudents = courseId => async () => {
	const { data } = await xhr.get(`/userlist/${courseId}`);

	return data;
};

export { getUser, updateProfile, getDocuments, getStudents };

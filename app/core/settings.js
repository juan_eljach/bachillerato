import moment from 'moment';

moment.locale('es');

export default {
	api: '/api'
};

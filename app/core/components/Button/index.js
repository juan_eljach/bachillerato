import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import Loading from 'core/components/Loading';

class Button extends Component {
	constructor (props) {
		super(props);
		this.state = {};
	}

	render () {
		const { children, type, disabled, onClick, isFetching, ...rest } = this.props;
		const buttonClassName = classnames(
			'button',
			{ 'button--disabled': disabled },
			{ 'button--secondary': type === 'secondary' }
		);

		return (
			<button className={buttonClassName} onClick={!disabled ? onClick : undefined} {...rest}>
				{isFetching ? <Loading type="spinner" /> : children}
			</button>
		);
	}
}

Button.propTypes = {
	text: PropTypes.string,
	disabled: PropTypes.bool
};

export default Button;

import React, { Component, createContext, Fragment } from 'react';
import classnames from 'classnames';
import Pagination from 'rc-pagination';

const TableContext = createContext();

const TableHead = ({ children }) => {
  let child = React.Children.map(children, c => {
    return React.cloneElement(c, { parent: 'head' });
  });

  return (
    <thead className="table__head">
      {child}
    </thead>
  );
};

const TableBody = ({ children }) => {
  let child = React.Children.map(children, c => {
    return React.cloneElement(c, { parent: 'body' });
  });

  return (
    <tbody className="table__body">
      {child}
    </tbody>
  );
};

const TableRow = ({ children, parent, section, ...rest }) => {
  return (
    <TableContext.Consumer>
      {(type) => {
        return (
          <tr className="table__row">
            {children}
          </tr>
        );
      }}
    </TableContext.Consumer>
  );
};

const TableCell = ({ children, ...rest }) => {
  return (
    <td className="table__cell">
      {children}
    </td>
  );
};

class Table extends Component {
  constructor (props) {
    super(props);
  }

  render () {
    const {
      children,
      type,
      totalItems,
      onPageChange
    } = this.props;

    const tableClassName = classnames(
      'table',
      { 'table-data-grid': type === 'data-grid' }
    );

    return (
      <TableContext.Provider value={type}>
        <table className={tableClassName}>{children}</table>
        {
          totalItems > 10 && (
            <Pagination
              total={totalItems}
              onChange={onPageChange}
            />
          )
        }
      </TableContext.Provider>
    );
  }
}

export {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell
};

export default Table;

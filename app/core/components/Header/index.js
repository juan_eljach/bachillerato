import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import Transition from 'core/components/Transition';
import Notification from './components/Notification';
import Modal from 'core/components/Modal';
import Messages from 'core/components/Messages';

import * as messagesActions from 'core/redux/messages/operations';
import * as userActions from 'core/redux/user/operations';

class Header extends Component {
	constructor (props) {
		super(props);
		this.state = {
			userMenuOpen: false,
			notificationOpen: false,
			messagesOpen: false
		};
	}

	async componentDidMount () {
		this.props.getUser();

		document.addEventListener('mousedown', this.handleShareClose, false);
	}

	componentWillUnmount = () => {
		document.removeEventListener('mousedown', this.handleShareClose, false);
	};

	handleShareClose = e => {
		this.setState({ notificationOpen: false });

		if (!this.menu) return;

		if (this.menu.contains(e.target)) {
			return;
		}

		this.setState({ userMenuOpen: false });
	};

	toggleUserMenu = () => {
		this.setState({ userMenuOpen: !this.state.userMenuOpen });
	};

	toggleNotification = () => {
		this.setState({ notificationOpen: !this.state.notificationOpen });
	};

	toggleMessages = () => {
		this.setState({ messagesOpen: !this.state.messagesOpen });
	};

	render () {
		const { userMenuOpen, notificationOpen, messagesOpen } = this.state;
		const { user, currentCourse } = this.props;

		return (
			<header className="header">
				<div className="logo">
					<Link to="/" className="v-align">
						<img src="/static/landing/img/logo.png" alt="Bachillerato desde casa" />
						<span className="show-for-medium">Instituto Virtual</span>
					</Link>
				</div>

				<ul className="header__comunication v-align">
					<li className="v-align" onClick={this.toggleNotification}>
						<i className="mdi mdi-bell" />
						<span className="show-for-large">Notificaciones</span>

						<Transition in={notificationOpen}>
							<Notification />
						</Transition>
					</li>
					<li>
						<a className="v-align" onClick={this.toggleMessages}>
							<i className="mdi mdi-email" />
							<span className="show-for-large">Mensajes</span>
						</a>
					</li>
				</ul>

				<div className="header__user" onClick={this.toggleUserMenu}>
					<img src={user.image ? user.image : '/static/app/img/user_avatar.png'} />
					<div>{`${user.first_name} ${user.last_name}`}</div>
				</div>

				<Transition in={userMenuOpen}>
					<div className="header__user-menu" ref={el => (this.menu = el)}>
						<ul>
							<li>
								<Link to="/profile" onClick={this.toggleUserMenu}>
									Mi Perfil
								</Link>
							</li>
							<li onClick={() => (window.location.href = '/accounts/logout')}>Cerrar sesión</li>
						</ul>
					</div>
				</Transition>

				<Modal open={messagesOpen}>
					<Messages
						user={this.props.user}
						getStudents={this.props.getStudents}
						getChats={this.props.getChats}
						getChatDetail={this.props.getChatDetail}
						sendMessage={this.props.sendMessage}
						toggleMessages={this.toggleMessages}
					/>
				</Modal>
			</header>
		);
	}
}

const mapStateToProps = ({ user, courses }) => {
	return {
		currentCourse: courses.current.id,
		user
	};
};

export default connect(
	mapStateToProps,
	{
		getUser: userActions.getUser,
		getStudents: userActions.getStudents,
		getChats: messagesActions.getChats,
		sendMessage: messagesActions.sendMessage,
		getChatDetail: messagesActions.getChatDetail
	}
)(Header);

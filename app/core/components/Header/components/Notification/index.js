import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Notification extends Component {
	constructor (props) {
		super(props);
		this.state = {};
	}

	render () {
		return (
			<div className="notification">
				<ul>
					<li className="notification__item">
						<Link to="/" className="v-align">
							<i className="mdi mdi-message-text-outline" />
							<div className="notification__item-info">
								<p>Notificación de mensaje</p>
								<span>hace 2 minutos</span>
							</div>
						</Link>
					</li>
					<li className="notification__item">
						<Link to="/" className="v-align">
							<i className="mdi mdi-check-circle-outline" />
							<div className="notification__item-info">
								<p>Notificación de tarea</p>
								<span>hace 5 minutos</span>
							</div>
						</Link>
					</li>
				</ul>
			</div>
		);
	}
}

export default Notification;

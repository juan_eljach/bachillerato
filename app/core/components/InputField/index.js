import React, { Component } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import TextField from './TextField';
import SelectField from './SelectField';
import TextAreaField from './TextAreaField';
import DateField from './DateField';

class InputField extends Component {
  constructor (props) {
    super(props);
    this.state = {};
  }

  render () {
    const { type, label, ...rest } = this.props;

    const inputFieldClassName = classnames(
      'input-field',
      { 'input-field--disabled': rest.disabled },
      { 'input-field--error': rest.meta && rest.meta.touched && rest.meta.error }
    );

    return (
      <div className={inputFieldClassName}>
        {
          label && <label className="input-field__label">{label}</label>
        }
        {
          type === 'text' && <TextField {...rest} />
        }

        {
          type === 'textarea' && <TextAreaField {...rest} />
        }

        {
          type === 'select' && <SelectField {...rest} />
        }

        {
          type === 'date' && <DateField {...rest} />
        }

        {rest.meta && rest.meta.touched && rest.meta.error && <div className="input-field__error">Campo requerido</div>}
      </div>
    );
  }
}

InputField.propTypes = {
  type: PropTypes.oneOf(['text', 'select', 'date', 'textarea'])
};

export default InputField;

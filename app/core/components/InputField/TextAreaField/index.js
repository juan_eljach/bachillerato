import React, { Component } from 'react';
import PropTypes from 'prop-types';

class TextArea extends Component {
  constructor (props) {
    super(props);
    this.state = {};
  }
  render () {
    const { placeholder, ...rest } = this.props;

    return (
      <div className="text-area">
        <textarea
          name=""
          rows="4"
          placeholder={placeholder}
          {...rest}>
        </textarea>
      </div>
    );
  }
}

TextArea.defaultProps = {
  placeholder: 'Escribe tu comentario'
};

TextArea.propTypes = {
  placeholder: PropTypes.string
};

export default TextArea;

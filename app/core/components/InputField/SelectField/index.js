import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Select from 'react-select';

class SelectField extends Component {
  constructor (props) {
    super(props);
    this.state = {};
  }
  render () {
    const { value, ...rest } = this.props;

    return (
      <Select
        value={value}
        classNamePrefix="select-field"
        isSearchable={false}
        {...rest}
      />
    );
  }
}

SelectField.defaultProps = {
  placeholder: 'Seleccionar'
};

SelectField.propTypes = {

};

export default SelectField;

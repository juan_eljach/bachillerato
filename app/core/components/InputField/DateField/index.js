import React, { Component } from 'react';
import PropTypes from 'prop-types';
import locale from 'rc-calendar/lib/locale/es_ES';
import Calendar from 'rc-calendar/lib/Calendar';
import Picker from 'rc-calendar/lib/Picker';

class DateField extends Component {
  renderDatePicker = (value, placeholder) => {
    return (
      <div className="date-field v-align" tabIndex="0">
        <i className="mdi mdi-calendar" />
        {!value ? (
          <span className="date-field__placeholder">{placeholder}</span>
        ) : (
            <span className="date-field__value">{value}</span>
          )}
        <i className="mdi mdi-chevron-down" />
      </div>
    );
  };

  render () {
    const { placeholder, icon, ...rest } = this.props;
    console.log(rest);
    const calendar = (
      <Calendar locale={locale} className="date-field__wrap" {...rest} />
    );
    const currentValue = rest.defaultValue || rest.value;
    const value = currentValue && currentValue.format('DD-MM-YYYY');

    if (rest.disabled) return this.renderDatePicker(value, placeholder);

    return (
      <Picker calendar={calendar} animation="slide-up">
        {() => this.renderDatePicker(value, placeholder)}
      </Picker>
    );
  }
}

DateField.propTypes = {
  defaultValue: PropTypes.object,
  placeholder: PropTypes.string,
  icon: PropTypes.bool
};

DateField.defaultProps = {
  placeholder: 'Seleccionar',
  icon: true
};

export default DateField;

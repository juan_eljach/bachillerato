import React, { Component } from 'react';
import PropTypes from 'prop-types';

class TextField extends Component {
  constructor (props) {
    super(props);
    this.state = {};
  }

  render () {
    const {...rest } = this.props;
    const { input, ...more } = rest;

    return (
      <div className="text-field">
        <input type="text" {...more} {...input} />
      </div>
    );
  }
}

TextField.propTypes = {

};

export default TextField;

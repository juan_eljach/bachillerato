import React, { Component } from 'react';
import ReactPagination from 'rc-pagination';
import PropTypes from 'prop-types';

class Pagination extends Component {
  constructor (props) {
    super(props);
    this.state = {};
  }

  render () {
    const { totalItems, perPage, current, onPageChange } = this.props;

    return (
      <ReactPagination
        current={current}
        total={totalItems}
        pageSize={perPage}
        onChange={onPageChange}
      />
    );
  }
}

Pagination.propTypes = {
  totalItems: PropTypes.number,
  perPage: PropTypes.number,
  onPageChange: PropTypes.func
};

export default Pagination;

import React, { Component } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

class Answer extends Component {
	constructor (props) {
		super(props);
		this.state = {};
	}
	render () {
		const { content, selected, sitting, isGivenAnswer, isRightAnswer, isCorrect, onClick } = this.props;

		const answerClassName = classnames('answer v-align', {
			'answer--selected': selected,
			'answer--disabled': sitting
		});
		const answerIconClassName = classnames('answer__icon v-align h-align', {
			'answer__icon--correct': sitting && isRightAnswer && isGivenAnswer,
			'answer__icon--correct-incorrect': sitting && !isRightAnswer && isCorrect,
			'answer__icon--incorrect': sitting && isGivenAnswer && !isRightAnswer
		});

		return (
			<div className={answerClassName} onClick={!sitting ? onClick : undefined}>
				<div className={answerIconClassName}>
					<i className={sitting && isRightAnswer ? 'mdi mdi-check' : 'mdi mdi-close'} />
				</div>
				<div className="answer__text">
					<p>{content}</p>
				</div>
			</div>
		);
	}
}

Answer.propTypes = {
	text: PropTypes.string,
	checked: PropTypes.bool,
	incorrect: PropTypes.bool,
	disabled: PropTypes.bool,
	onClick: PropTypes.func
};

export default Answer;

import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Loading extends Component {
	constructor (props) {
		super(props);
		this.state = {};
	}

	renderLoading = type => {
		if (type === 'circles') {
			return (
				<div className="loading-circles">
					<svg
						version="1.1"
						id="L4"
						xmlns="http://www.w3.org/2000/svg"
						x="0px"
						y="0px"
						viewBox="0 0 60 60"
						enableBackground="new 0 0 0 0"
					>
						<circle fill="#fff" stroke="none" cx="6" cy="50" r="6">
							<animate
								attributeName="opacity"
								dur="1s"
								values="0;1;0"
								repeatCount="indefinite"
								begin="0.1"
							/>
						</circle>
						<circle fill="#fff" stroke="none" cx="26" cy="50" r="6">
							<animate
								attributeName="opacity"
								dur="1s"
								values="0;1;0"
								repeatCount="indefinite"
								begin="0.2"
							/>
						</circle>
						<circle fill="#fff" stroke="none" cx="46" cy="50" r="6">
							<animate
								attributeName="opacity"
								dur="1s"
								values="0;1;0"
								repeatCount="indefinite"
								begin="0.3"
							/>
						</circle>
					</svg>
				</div>
			);
		}

		if (type === 'spinner') {
			return <div className="spinner" />;
		}

		if (type === 'loading') {
			return (
				<div className="loading">
					<div className="loading__showbox">
						<div className="loading__loader">
							<svg className="loading__circular" viewBox="25 25 50 50">
								<circle
									className="loading__path"
									cx="50"
									cy="50"
									r="20"
									fill="none"
									strokeWidth="3"
									strokeMiterlimit="10"
								/>
							</svg>
						</div>
					</div>
				</div>
			);
		}
	};

	render () {
		return this.renderLoading(this.props.type);
	}
}

Loading.defaultProps = {
	type: 'circles'
};

Loading.propTypes = {
	type: PropTypes.string
};

export default Loading;

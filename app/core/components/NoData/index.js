import React, { Component } from 'react';
import PropTypes from 'prop-types';

class NoData extends Component {
	constructor (props) {
		super(props);
		this.state = {};
	}
	render () {
		return <div className="no-data">{this.props.text}</div>;
	}
}

NoData.propTypes = {
	text: PropTypes.string
};

export default NoData;

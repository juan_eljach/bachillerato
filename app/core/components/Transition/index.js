import React, { Component } from 'react';
import { CSSTransition } from 'react-transition-group';

class Transition extends Component {
  constructor (props) {
    super(props);
    this.state = {};
  }
  render () {
    const { children, in: inProp, timeout } = this.props;

    return (
      <CSSTransition
        in={inProp}
        timeout={timeout}
        classNames="bounce"
        unmountOnExit
      >

      {
        (state) => (
          children
        )
      }

      </CSSTransition>
    );
  }
}

Transition.defaultProps = {
  timeout: 300
};

export default Transition;

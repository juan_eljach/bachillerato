import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import uuid from 'uuid/v4';

import Button from 'core/components/Button';
import InputField from 'core/components/InputField';
import Searcher from 'core/components/Searcher';
import UserRow from './UserRow';
import UserMessage from './UserMessage';

class Messages extends Component {
	constructor (props) {
		super(props);
		this.state = {
			section: 'chats',
			students: [],
			chats: [],
			selectedStudent: {},
			messages: [],
			inputValue: '',
			searchValue: '',
			isSearching: false
		};
	}

	async componentDidMount () {
		const students = await this.props.getStudents(this.props.user.current_grade);
		const chats = await this.props.getChats();

		this.setState({ students, chats });
	}

	componentDidUpdate (prevProps, prevState) {
		if (['chats', 'newMessage'].includes(prevState.section) || this.state.section === 'messages') {
			this.scrollToBottom();
		}
	}

	onSectionChange = async (section, data) => {
		this.setState({ section });

		if (section === 'messages') {
			this.setState({ selectedStudent: data });

			const response = await this.props.getChatDetail(data.chat);

			this.setState({ messages: response.messages });
		}
	};

	onSearch = value => {
		this.setState(() => {
			return {
				searchValue: value
			};
		});
	};

	scrollToBottom () {
		if (this.chatBody) {
			this.chatBody.scrollTop = this.chatBody.scrollHeight;
		}
	}

	onInputChange = event => {
		this.setState({ inputValue: event.target.value });
	};

	onSendMessage = async () => {
		this.setState(
			prevState => {
				const newMessage = {
					id: uuid(),
					body: this.state.inputValue,
					sender: this.props.user.id,
					sent_at: null
				};
				return {
					...prevState,
					messages: [...prevState.messages, newMessage],
					chats: prevState.chats.map((c, i) => {
						if (prevState.chats.length === i + 1) {
							return { ...c, messages: [...c.messages, newMessage] };
						}
					}),
					inputValue: ''
				};
			},
			() => this.scrollToBottom()
		);

		await this.props.sendMessage({
			body: this.state.inputValue,
			members: [this.props.user.id, this.state.selectedStudent.pk]
		});
	};

	render () {
		const {
			section,
			chats,
			students,
			searchValue,
			inputValue,
			selectedStudent,
			messages,
			isSearching
		} = this.state;
		const { toggleMessages, user } = this.props;

		return (
			<div className="messages">
				<div className="messages__header v-align space-between">
					{section === 'chats' && (
						<Fragment>
							<h4>Mensajes directos</h4>
							<div className="messages__header-actions">
								<Button onClick={() => this.onSectionChange('newMessage')}>Mensaje nuevo</Button>
								<i className="mdi mdi-close action-hover" onClick={toggleMessages} />
							</div>
						</Fragment>
					)}

					{(section === 'newMessage' || section === 'messages') && (
						<Fragment>
							<i
								className="mdi mdi-chevron-left action-hover"
								onClick={() => this.onSectionChange('chats')}
							/>

							{section === 'newMessage' && <h4>Mensajes nuevo</h4>}

							{section === 'messages' && (
								<div className="messages__header-current-user v-align">
									<img src="/static/app/img/user_avatar.png" alt="" />
									<div>{selectedStudent.full_name}</div>
								</div>
							)}

							<i className="mdi mdi-close action-hover" onClick={toggleMessages} />
						</Fragment>
					)}
				</div>

				<div className="messages__body">
					{section === 'newMessage' && (
						<div className="messages__search">
							<div className="messages__search-input">
								<Searcher
									placeholder="Buscar usuarios del curso..."
									onInputChange={this.onSearch}
									menuIsOpen={false}
									isLoading={isSearching}
								/>
							</div>

							<div className="messages__search-list">
								{students.map(student => {
									const chat = chats.find(c => c.members.includes(student.pk));

									if (
										student.full_name.toLowerCase().search(searchValue.toLowerCase()) === -1
										|| user.id === student.pk
									) {
										return null;
									}

									return (
										<UserRow
											key={student.pk}
											onClick={() =>
												this.onSectionChange('messages', { ...student, chat: chat && chat.id })
											}
											name={student.full_name}
											search
										/>
									);
								})}
							</div>
						</div>
					)}

					{section === 'chats' && (
						<div className="messages__chats">
							{chats.map(chat => {
								const lastMessage = chat.messages[chat.messages.length - 1];
								const student = students.find(stud => stud.pk === chat.members[1]);

								return (
									<UserRow
										key={chat.id}
										message={lastMessage.body}
										name={student.full_name}
										date={lastMessage.date}
										current={lastMessage.sender === user.id}
										onClick={() => this.onSectionChange('messages', { ...student, chat: chat.id })}
									/>
								);
							})}
						</div>
					)}

					{section === 'messages' && (
						<div className="messages__chat">
							<div className="messages__chat-body" ref={el => (this.chatBody = el)}>
								{messages.map(message => {
									if (message.sender === user.id) {
										return (
											<UserMessage
												key={message.id}
												body={message.body}
												sentAt={message.sent_at}
												current
											/>
										);
									}

									return (
										<UserMessage key={message.id} body={message.body} sentAt={message.sent_at} />
									);
								})}
							</div>

							<div className="messages__chat-composer v-align">
								<InputField
									value={inputValue}
									type="textarea"
									rows={1}
									placeholder="Escribir mensaje..."
									onChange={this.onInputChange}
								/>
								<Button onClick={this.onSendMessage} disabled={!inputValue}>
									Enviar
								</Button>
							</div>
						</div>
					)}
				</div>
			</div>
		);
	}
}

Messages.propTypes = {
	toggleMessages: PropTypes.func.isRequired
};

export default Messages;

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

class UserMessage extends Component {
	constructor (props) {
		super(props);
		this.state = {};
	}
	render () {
		const { current, body, sentAt } = this.props;

		const userMessageClassName = classnames('user-message v-align', { 'user-message--current': current });

		return (
			<div className={userMessageClassName}>
				<img src="/static/app/img/user_avatar.png" alt="" />
				<div className="user-message__text">
					<p>{body}</p>
					<span>22 oct. 2018</span>
				</div>
			</div>
		);
	}
}

export default UserMessage;

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

class UserRow extends Component {
	constructor (props) {
		super(props);
		this.state = {};
	}
	render () {
		const { name, message, date, search, current, ...rest } = this.props;
		const userRowClassName = classnames('user-row v-align', { 'user-row--search': search });

		return (
			<div className={userRowClassName} {...rest}>
				<div className="user-row__avatar">
					<img src="/static/app/img/user_avatar.png" alt="" />
				</div>
				<div className="user-row__info">
					<div className="user-row__name">
						{current && 'Tu: '}
						{name}
					</div>
					<p className="user-row__answer">{message}</p>
				</div>

				<div className="user-row__date">{date}</div>
			</div>
		);
	}
}

UserRow.propTypes = {
	name: PropTypes.string,
	search: PropTypes.bool,
	onClick: PropTypes.func.isRequired
};

export default UserRow;

import axios from 'axios';
import getCookie from 'core/helpers/getCookie';

import settings from './settings';

const csrftoken = getCookie('csrftoken');

axios.defaults.baseURL = '/';
axios.defaults.headers.common['X-CSRFToken'] = csrftoken;

const xhrApi = axios.create({
	baseURL: settings.api,
	headers: { 'X-CSRFToken': csrftoken }
});

export { xhrApi };
export default axios;

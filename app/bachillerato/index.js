import './bachillerato.scss';

import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import store from './store';

import AppRouter from './routes';

import Header from 'core/components/Header';

const App = () => {
  return (
    <Provider store={store}>
      <section className="bachillerato">
        <AppRouter>
          <Header />
        </AppRouter>
      </section>
    </Provider>
  );
};

render(<App />, document.querySelector('#App'));

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Line } from 'rc-progress';
import { Link } from 'react-router-dom';
import { debounce as debounceFunc } from 'throttle-debounce';

import Table, { TableHead, TableBody, TableRow, TableCell } from 'core/components/Table';
import Pagination from 'core/components/Pagination';
import Tooltip from 'core/components/Tooltip';
import Searcher from 'core/components/Searcher';
import * as userActions from 'core/redux/user/operations';
import { getStudents } from '../../../core/redux/user/operations';

class Students extends Component {
	constructor (props) {
		super(props);
		this.state = {
			isSearching: false
		};

		this.onInputChange = debounceFunc(500, this.onInputChange.bind(this));
	}

	onInputChange = () => {
		this.setState({ isSearching: true });

		this.props.getStudents(this.props.user.current_grade);

		this.setState({ isSearching: false });
	};

	render () {
		return (
			<section className="students wrap-container">
				<h2>
					Estudiantes matriculados en <strong>Sexto</strong>
				</h2>
				<div className="students__container">
					<div className="students__header">Estudiantes online de tu curso</div>

					<div className="students__search">
						<Searcher placeholder="Buscar..." onInputChange={this.onInputChange} isLoading />
					</div>

					<div className="students__table">
						<Table type="data-grid">
							<TableHead>
								<TableRow>
									<TableCell>Estudiante</TableCell>
									<TableCell>Progreso</TableCell>
									<TableCell />
								</TableRow>
							</TableHead>
							<TableBody>
								<TableRow>
									<TableCell>John García</TableCell>
									<TableCell>
										<div className="students__progress">
											<Line percent={50} strokeWidth="2" trailWidth="2" strokeColor="#5e43c0" />
										</div>
									</TableCell>
									<TableCell>
										<Link to="/messages">
											<Tooltip title="Enviar mensaje">
												<i className="mdi mdi-email" />
											</Tooltip>
										</Link>
									</TableCell>
								</TableRow>

								<TableRow>
									<TableCell>Diego García</TableCell>
									<TableCell>
										<div className="students__progress">
											<Line percent={50} strokeWidth="2" trailWidth="2" strokeColor="#5e43c0" />
										</div>
									</TableCell>
									<TableCell>
										<Link to="/messages">
											<Tooltip title="Enviar mensaje">
												<i className="mdi mdi-email" />
											</Tooltip>
										</Link>
									</TableCell>
								</TableRow>
							</TableBody>
						</Table>

						<Pagination current={1} totalItems={12} perPage={6} onPageChange={this.onQuestionChange} />
					</div>
				</div>
			</section>
		);
	}
}

const mapStateToProps = ({ user }) => {
	return {
		user
	};
};

export default connect(
	mapStateToProps,
	{
		getStudents: userActions.getStudents
	}
)(Students);

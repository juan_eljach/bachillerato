import React, { Component } from 'react';
import { Editor } from 'react-draft-wysiwyg';
import classnames from 'classnames';

import Table, { TableHead, TableBody, TableRow, TableCell } from 'core/components/Table';
import InputField from 'core/components/InputField';
import Button from 'core/components/Button';

class Messages extends Component {
	constructor (props) {
		super(props);
		this.state = {
			selectedTab: 'write',
			editorState: ''
		};

		this.tabs = [
			{ value: 'write', label: 'Enviar Nuevo Mensaje', icon: 'plus' },
			{ value: 'inbox', label: 'Bandeja de entrada', icon: 'inbox' },
			{ value: 'sent', label: 'MEnsajes Enviados', icon: 'send' }
		];
	}

	onTabChange = tab => {
		this.setState({ selectedTab: tab });
	};

	onEditorStateChange = value => {
		this.setState({ editorState: value });
	};

	render () {
		const { editorState, selectedTab } = this.state;

		return (
			<section className="messages-prev wrap-container">
				<div className="messages-prev-sidebar">
					<div className="messages-prev-sidebar__header">
						<h4>CHATEA CON NUESTROS PROFESORES</h4>
					</div>

					<div className="messages-prev-sidebar__content">
						<nav className="messages-prev-sidebar__nav">
							<ul>
								{this.tabs.map(tab => {
									const tabClassName = classnames('messages-prev-sidebar__nav-item v-align', {
										'messages-prev-sidebar__nav-item--active': tab.value === selectedTab
									});

									return (
										<li
											key={tab.value}
											className={tabClassName}
											onClick={() => this.onTabChange(tab.value)}
										>
											<div className="messages-prev-sidebar__nav-item-icon h-align v-align">
												<i className={`mdi mdi-${tab.icon}`} />
											</div>
											<span>{tab.label}</span>
										</li>
									);
								})}
							</ul>
						</nav>

						<h5>Docentes de tu Curso</h5>

						<div className="messages-prev-sidebar__teachers">
							<div className="messages-prev-sidebar__teacher v-align">
								<img
									src="https://pbs.twimg.com/profile_images/712466126450483200/wmkqffYN_400x400.jpg"
									alt=""
								/>
								<div className="messages-prev-sidebar__teacher-name">
									<div>Dan Amabrov</div>
									<span>Matemáticas</span>
								</div>
							</div>

							<div className="messages-prev-sidebar__teacher v-align">
								<img
									src="https://pbs.twimg.com/profile_images/712466126450483200/wmkqffYN_400x400.jpg"
									alt=""
								/>
								<div className="messages-prev-sidebar__teacher-name">
									<div>Peter Thiel</div>
									<span>Biología</span>
								</div>
							</div>

							<div className="messages-prev-sidebar__teacher v-align">
								<img
									src="https://pbs.twimg.com/profile_images/712466126450483200/wmkqffYN_400x400.jpg"
									alt=""
								/>
								<div className="messages-prev-sidebar__teacher-name">
									<div>Sean Parker</div>
									<span>Literatura</span>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div className="messages-prev-main">
					<h4>BANDEJA DE ENTRADA</h4>
					{selectedTab === 'write' && (
						<div className="messages-prev-write">
							<div className="messages-prev-write__header v-align">
								<img
									src="https://pbs.twimg.com/profile_images/712466126450483200/wmkqffYN_400x400.jpg"
									alt=""
								/>
								<div>
									<div>
										Mensaje para: <strong>Andrew Simon</strong>
									</div>
									<span>Docente (Matemáticas)</span>
								</div>
							</div>

							<form className="messages-prev-write__form">
								<InputField type="text" placeholder="Asunto:" />

								<div>
									<Editor
										editorState={editorState}
										toolbarClassName="toolbarClassName"
										wrapperClassName="wrapperClassName"
										editorClassName="editorClassName"
										onEditorStateChange={this.onEditorStateChange}
									/>
								</div>

								<div className="messages-prev-write__form-button">
									<Button>Enviar</Button>
								</div>
							</form>
						</div>
					)}

					{(selectedTab === 'inbox' || selectedTab === 'sent') && (
						<div className="messages-prev-table">
							<Table type="data-grid">
								<TableHead>
									<TableRow>
										<TableCell>No.</TableCell>
										<TableCell>Usuario</TableCell>
										<TableCell>Asunto</TableCell>
										<TableCell>Fecha</TableCell>
										<TableCell>Estado</TableCell>
									</TableRow>
								</TableHead>
								<TableBody>
									<TableRow>
										<TableCell>1</TableCell>
										<TableCell>Elon Musk</TableCell>
										<TableCell>Pregunta</TableCell>
										<TableCell>26-08-2018</TableCell>
										<TableCell>--</TableCell>
									</TableRow>
								</TableBody>
							</Table>
						</div>
					)}
				</div>
			</section>
		);
	}
}

export default Messages;

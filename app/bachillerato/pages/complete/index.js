import React, { Component } from 'react';

import InputField from 'core/components/InputField';
import Button from 'core/components/Button';
import { courses } from 'core/data/users';

class Complete extends Component {
  constructor (props) {
    super(props);
    this.state = {};
  }
  render () {
    return (
      <section className="complete wrap-container">
        <h2>Para continuar completa la siguiente información:</h2>
        <div className="complete__fields">
          <InputField
            type="select"
            label="Último grado aprobado"
            options={courses}
          />

          <div className="h-align">
            <Button>Enviar</Button>
          </div>
        </div>
      </section>
    );
  }
}

export default Complete;

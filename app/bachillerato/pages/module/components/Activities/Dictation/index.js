import React, { Component } from 'react';
import ReactAudioPlayer from 'react-audio-player';
import { diffWords } from 'diff';
import classnames from 'classnames';

import Button from 'core/components/Button';
import InputField from 'core/components/InputField';

class Dictation extends Component {
	constructor (props) {
		super(props);
		this.state = {
			answer: '',
			correctAnswer: '',
			diff: null,
			renderSection: false,
			isFetching: false
		};
	}

	async componentDidMount () {
		const { sitting, id } = this.props.activity;

		if (sitting) {
			const fullDisplayResponse = await this.props.getFullDisplayDictation(id);

			const diff = this.getDiff(sitting.answer, fullDisplayResponse.audio_to_text);

			return this.setState({ diff, correctAnswer: fullDisplayResponse.audio_to_text, renderSection: true });
		}

		this.setState({ renderSection: true });
	}

	onInputChange = ({ target }) => {
		this.setState({ answer: target.value });
	};

	onEnded = () => {};

	getDiff = (answerText, correctText) => {
		const diff = diffWords(correctText, answerText);

		return diff;
	};

	onRespond = async () => {
		const fullDisplayResponse = await this.props.getFullDisplayDictation(this.props.activity.id);

		this.setState({ isFetching: true });

		const diff = this.getDiff(this.state.answer, fullDisplayResponse.audio_to_text);

		const totalWords = fullDisplayResponse.audio_to_text.trim().split(' ').length;
		const hits = diff.reduce((acc, current) => {
			if (!current.removed && !current.added) {
				return acc + current.value.trim().split(' ').length;
			}

			return acc;
		}, 0);

		let payload = {
			grade: Math.round(((hits * 100) / totalWords) * 100) / 100,
			number_of_errors: totalWords - hits,
			answer: this.state.answer,
			dictation: this.props.activity.id
		};

		const dictationResponse = await this.props.respondDictation(payload);

		this.props.updateActivity('dictations', { ...this.props.activity, sitting: { ...dictationResponse } });

		if (dictationResponse) {
			this.setState({ diff, correctAnswer: fullDisplayResponse.audio_to_text, isFetching: false });
		}
	};

	render () {
		const { answer, correctAnswer, diff, renderSection, isFetching } = this.state;
		const { activity, goToSection } = this.props;

		if (!renderSection) return null;

		return (
			<section className="dictation">
				<p>
					Escuche atentamente el audio y escriba en el cuadro de texto lo que escuche, incluyéndo signos de
					puntuación:
				</p>

				<ReactAudioPlayer src={activity.audio} onEnded={this.onEnded} controls />

				{!diff ? (
					<InputField
						type="textarea"
						placeholder="Escriba el dictado en el audio que encuentra adjunto."
						onChange={this.onInputChange}
					/>
				) : (
					<div className="dictation__diff v-align space-between">
						<div>
							<h5>Tu respuesta</h5>
							<div>
								{diff.map((part, index) => {
									if (part.removed) return null;

									const charClassName = classnames({
										'dictation__diff--wrong': part.added || part.removed
									});

									return (
										<span className={charClassName} key={index}>
											{part.value}
										</span>
									);
								})}
							</div>
						</div>

						<div>
							<h5>Respuesta correcta</h5>
							{correctAnswer}
						</div>
					</div>
				)}

				<div className="dictation__buttons">
					{!diff && (
						<Button disabled={!answer} onClick={this.onRespond} isFetching={isFetching}>
							Responder
						</Button>
					)}
				</div>
			</section>
		);
	}
}

export default Dictation;

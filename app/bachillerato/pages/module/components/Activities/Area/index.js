import React, { Component } from 'react';
import ImageMapper from './ImageMapper';

class Area extends Component {
	constructor (props) {
		super(props);
		this.state = {
			hoveredArea: { name: '1', shape: 'circle', coords: [143, 222, 25] }
		};
	}

	onZoneHover = (area, index, event) => {
		console.log(area);
		console.log(index);
		console.log(event.nativeEvent);
	};

	render () {
		const map = {
			name: 'my-map',
			areas: [{ name: '1', shape: 'circle', coords: [120, 220, 25] }]
		};

		return (
			<section className="area">
				<p>Para verificar tus respuestas pasa el mouse sobre las areas sombreadas:</p>

				<div className="area__answer">
					<span>Higado</span>
					<i className="mdi mdi-check-circle-outline" />
				</div>

				<div className="area__image">
					<ImageMapper
						src="/static/app/img/area.jpg"
						map={map}
						fillColor="rgba(251, 140, 85, .6)"
						onMouseEnter={this.onZoneHover}
					/>
				</div>
			</section>
		);
	}
}

export default Area;

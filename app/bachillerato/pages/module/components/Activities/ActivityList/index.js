import React, { Component } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import ActivityItem from './ActivityItem';

class ActivityList extends Component {
	constructor (props) {
		super(props);
		this.state = {
			activities: [
				{ text: 'Actividad de Selección', id: 1, score: 0, section: 'selection' },
				{ text: 'Actividad Dictado', id: 4, score: 0, section: 'dictation' },
				{ text: 'Pregunta de Relación', id: 2, score: 0, section: 'relation' },
				{ text: 'Actividad de Área', id: 5, score: 0, section: 'area' }
			]
		};
	}

	render () {
		const { activities } = this.state;
		const { mc_questions, dictations, relationships, goToSection, activitiesDone } = this.props;

		return (
			<div className="activity-list">
				<ul>
					{mc_questions.map(activity => {
						return (
							<ActivityItem
								key={activity.id}
								activity={activity}
								goToSection={() => goToSection('selection', activity)}
							/>
						);
					})}

					{dictations.map(activity => {
						return (
							<ActivityItem
								key={activity.id}
								activity={activity}
								goToSection={() => goToSection('dictation', activity)}
							/>
						);
					})}

					{relationships.map(activity => {
						return (
							<ActivityItem
								key={activity.id}
								activity={activity}
								goToSection={() => goToSection('relationship', activity)}
							/>
						);
					})}
				</ul>
			</div>
		);
	}
}

ActivityList.propTypes = {};

export default ActivityList;

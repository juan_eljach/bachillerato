import React, { Component } from 'react';
import classnames from 'classnames';

class ActivityItem extends Component {
	constructor (props) {
		super(props);
		this.state = {};
	}
	render () {
		const activitiesDone = [];
		const { activity, goToSection } = this.props;

		const itemStatusClassName = classnames('activity-list__item-status v-align h-align', {
			'activity-list__item-status--done': activity.sitting
		});

		return (
			<li className="activity-list__item" key={activity.id}>
				<div className={itemStatusClassName}>
					<i className="mdi mdi-check-circle" />
				</div>
				<p className="activity-list__item-title" onClick={goToSection}>
					{activity.title}
				</p>
				<div className="activity-list__item-score">{activitiesDone.includes(activity.id) ? 100 : 0}</div>
			</li>
		);
	}
}

export default ActivityItem;

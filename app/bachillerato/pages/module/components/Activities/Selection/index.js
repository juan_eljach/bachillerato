import React, { Component } from 'react';
import classnames from 'classnames';

import Button from 'core/components/Button';
import Answer from 'core/components/Answer';

class SelectionActivity extends Component {
	constructor (props) {
		super(props);
		this.state = {
			selectedAnswer: {},
			isFetching: false
		};
	}

	onSelectAnswer = answer => {
		console.log(answer);
		if (this.props.activity.sitting) {
			return;
		}

		this.setState({ selectedAnswer: answer });
	};

	sendAnswer = async () => {
		let payload = {
			mc_question: this.props.activity.id,
			given_answer: this.state.selectedAnswer.id
		};

		this.setState({ isFetching: true });

		const response = await this.props.respondSelection(payload);

		this.setState({ isFetching: false });

		if (response) {
			this.props.updateActivity('mc_questions', { ...this.props.activity, sitting: { ...response } });
			this.props.goToSection('list');
		}
	};

	render () {
		const { selectedAnswer, isFetching } = this.state;
		const { activity } = this.props;

		return (
			<div className="selection-activity">
				<h5>{activity.content}</h5>

				<div className="selection-activity__title">{activity.explanation}</div>

				<div className="selection-activity__answers-list">
					{activity.answers.map(answer => {
						const { sitting } = activity;

						return (
							<Answer
								key={answer.id}
								content={answer.content}
								selected={answer.id === selectedAnswer.id}
								sitting={!!sitting}
								isGivenAnswer={sitting && answer.id === sitting.given_answer}
								isRightAnswer={sitting && sitting.is_right_answer}
								isCorrect={sitting && answer.correct}
								onClick={() => this.onSelectAnswer(answer)}
							/>
						);
					})}
				</div>

				{!activity.sitting && (
					<div className="selection-activity__buttons">
						<Button onClick={() => this.sendAnswer(selectedAnswer)} isFetching={isFetching}>
							Responder
						</Button>
					</div>
				)}
			</div>
		);
	}
}

export default SelectionActivity;

import React, { Component, Fragment } from 'react';
import { Droppable } from 'react-beautiful-dnd';

class ContentCard extends Component {
	constructor (props) {
		super(props);
		this.state = {};
	}
	render () {
		const { droppableId, disabled, content } = this.props;

		return (
			<Droppable droppableId={droppableId} isDropDisabled={disabled}>
				{provided => (
					<div className="content-card v-align" ref={provided.innerRef}>
						{this.props.children}
						{provided.placeholder}
					</div>
				)}
			</Droppable>
		);
	}
}

export default ContentCard;

import React, { Component } from 'react';
import { Droppable } from 'react-beautiful-dnd';

import ContentElement from './ContentElement';

class AnswersList extends Component {
	constructor (props) {
		super(props);
		this.state = {};
	}
	render () {
		const { answerList } = this.props;

		return (
			<Droppable droppableId="AnswersList" direction="horizontal">
				{provided => {
					return (
						<div className="answers-list v-align" ref={provided.innerRef}>
							{answerList.map((answer, index) => {
								return (
									<ContentElement
										key={`answer-${answer.answerId}`}
										{...answer}
										answerId={answer.answerId}
									/>
								);
							})}
							{provided.placeholder}
						</div>
					);
				}}
			</Droppable>
		);
	}
}

export default AnswersList;

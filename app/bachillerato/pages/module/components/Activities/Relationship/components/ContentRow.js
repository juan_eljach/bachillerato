import React, { Component } from 'react';

import ContentCard from './ContentCard';
import ContentElement from './ContentElement';
import ArrowRow from './ArrowRow';

class ContentColumn extends Component {
	constructor (props) {
		super(props);
		this.state = {};
	}
	render () {
		const { columns, rowId, sitting } = this.props;

		return (
			<div className="content-row">
				<ContentCard droppableId="card1" disabled>
					<ContentElement
						content={columns.a.content}
						isImage={columns.a.isImage}
						answerId={`a-${columns.a.answerId}`}
						disabled
					/>
				</ContentCard>
				<ArrowRow>
					{sitting
						&& sitting.right_relationships.includes(columns.a.answerId) && (
							<i className="mdi mdi-check-circle" />
						)}
					{sitting
						&& sitting.wrong_relationships.includes(columns.a.answerId) && (
							<i className="mdi mdi-close-circle" />
						)}
				</ArrowRow>
				<ContentCard droppableId={rowId}>
					<ContentElement
						content={columns.b.content}
						isImage={columns.b.isImage}
						answerId={`b-${columns.b.answerId}`}
						disabled={sitting}
					/>
				</ContentCard>
			</div>
		);
	}
}

export default ContentColumn;

import React, { Component } from 'react';

class ArrowRow extends Component {
	state = {};
	render () {
		return (
			<div className="arrow-row">
				<span />
				<i className="mdi mdi-chevron-right" />

				{this.props.children}
			</div>
		);
	}
}

export default ArrowRow;

import React, { Component } from 'react';
import { Draggable } from 'react-beautiful-dnd';

class ContentElement extends Component {
	constructor (props) {
		super(props);
		this.state = {};
	}
	render () {
		const { content, isImage, answerId, disabled } = this.props;

		return (
			<Draggable draggableId={answerId} isDragDisabled={disabled}>
				{provided => {
					return (
						<div
							className="content-element"
							ref={provided.innerRef}
							{...provided.draggableProps}
							{...provided.dragHandleProps}
						>
							{!isImage ? <span>{content}</span> : <img src={content} />}
						</div>
					);
				}}
			</Draggable>
		);
	}
}

export default ContentElement;

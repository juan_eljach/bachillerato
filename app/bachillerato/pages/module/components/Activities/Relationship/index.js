import React, { Component } from 'react';
import { DragDropContext, Droppable } from 'react-beautiful-dnd';
import uuid from 'uuid/v4';

import AnswersList from './components/AnswersList';
import ContentRow from './components/ContentRow';
import Button from 'core/components/Button/index';

class Relationship extends Component {
	constructor (props) {
		super(props);
		this.state = {
			answerList: {},
			rowContent: {},
			renderSection: false
		};

		this.data = {
			contentRow: [{ answer: 1, column_b_content: 'texto' }, { answer: 2, column_b_content: 'texto' }],
			contentColumns: {
				1: [{ column_a_content: 'texto' }, { column_b_content: '' }]
			}
		};
	}

	componentDidMount () {
		const { activity } = this.props;
		const relationship = this.transformAnswers(activity.answers);

		let rowContent;

		// if (activity.sitting) {
		// 	let userResponse = JSON.parse(activity.sitting.user_response);

		// 	rowContent = { ...relationship.rowContent, user_response: userResponse };
		// } else {
		// 	rowContent = relationship.rowContent;
		// }

		rowContent = relationship.rowContent;

		console.log(rowContent);
		console.log(relationship.answerList);

		this.setState({ answerList: relationship.answerList, rowContent, renderSection: true });
	}

	transformAnswers = answers => {
		const answerList = answers.map(answer => {
			return {
				content: answer.column_b_content || answer.column_b_image,
				isImage: !!answer.column_b_image,
				answerId: answer.id
			};
		});

		const rowContent = answers.map(answer => {
			let b;

			if (this.props.activity.sitting) {
				let userResponse = JSON.parse(this.props.activity.sitting.user_response);
				let relatedAnswer = answers.find(a => a.id === userResponse[answer.id]);

				if (relatedAnswer) {
					b = {
						content: answer.column_b_content || answer.column_b_image,
						isImage: !!answer.column_b_image,
						answerId: answer.id
					};
				}
			}

			return {
				columns: {
					a: {
						content: answer.column_a_content || answer.column_a_image,
						isImage: !!answer.column_a_image,
						answerId: answer.id
					},
					b: b || {}
				},
				rowId: uuid()
			};
		});

		return { answerList, rowContent };
	};

	onDragEnd = provided => {
		if (!provided.source || !provided.destination) return;
		if (provided.source.droppableId === provided.destination.droppableId) return;

		if (provided.source.droppableId === 'AnswersList') {
			const taken = this.state.rowContent.find(rc => rc.rowId === provided.destination.droppableId).columns.b;

			if (taken.answerId) return;

			this.setState(prevState => {
				return {
					answerList: prevState.answerList.filter(a => a.answerId !== provided.draggableId),
					rowContent: prevState.rowContent.map(c => {
						if (c.rowId === provided.destination.droppableId) {
							return {
								...c,
								columns: {
									...c.columns,
									b: prevState.answerList.find(a => a.answerId === provided.draggableId)
								}
							};
						}

						return c;
					})
				};
			});

			return;
		}

		if (provided.destination.droppableId === 'AnswersList') {
			this.setState(prevState => {
				return {
					...prevState,
					answerList: [
						...prevState.answerList,
						prevState.rowContent.find(c => c.rowId === provided.source.droppableId).columns.b
					],
					rowContent: prevState.rowContent.map(c => {
						if (c.rowId === provided.source.droppableId) {
							return {
								...c,
								columns: {
									...c.columns,
									b: {}
								}
							};
						}

						return c;
					})
				};
			});

			return;
		}

		if (provided.destination.droppableId !== 'AnswersList') {
			this.setState(prevState => {
				return {
					...prevState,
					rowContent: prevState.rowContent.map(c => {
						if (c.rowId === provided.source.droppableId) {
							const destinationContent = prevState.rowContent.find(
								rc => rc.rowId === provided.destination.droppableId
							);
							return {
								...c,
								columns: {
									...c.columns,
									b: destinationContent.columns.b
								}
							};
						}

						if (c.rowId === provided.destination.droppableId) {
							const sourceContent = prevState.rowContent.find(
								rc => rc.rowId === provided.source.droppableId
							);
							return {
								...c,
								columns: {
									...c.columns,
									b: sourceContent.columns.b
								}
							};
						}

						return c;
					})
				};
			});
		}
	};

	sendResponse = async () => {
		const { rowContent } = this.state;

		this.setState({ isFetching: true });

		const relationships = rowContent.reduce(
			(acc, current) => {
				let user_response = { ...acc.user_response, [current.columns.a.answerId]: current.columns.b.answerId };

				if (current.columns.a.answerId === current.columns.b.answerId) {
					return {
						...acc,
						right_relationships: [...acc.right_relationships, current.columns.a.answerId],
						user_response
					};
				}

				return {
					...acc,
					wrong_relationships: [...acc.wrong_relationships, current.columns.a.answerId],
					user_response
				};
			},
			{ right_relationships: [], wrong_relationships: [], user_response: {} }
		);

		const response = await this.props.respondRelationship({
			relationship_question: this.props.activity.id,
			...relationships,
			user_response: JSON.stringify(relationships.user_response)
		});

		this.setState({ isFetching: false });

		this.props.updateActivity('relationships', { ...this.props.activity, sitting: { ...response } });

		this.props.goToSection('list');
	};

	render () {
		const { renderSection, answerList, rowContent } = this.state;
		const { activity } = this.props;

		if (!renderSection) return null;

		const related = rowContent.reduce((acc, current) => {
			return acc && current.columns.b.answerId;
		}, true);

		return (
			<section className="relationship">
				<DragDropContext onDragEnd={this.onDragEnd}>
					{!activity.sitting && (
						<div className="relationship__header v-align">
							<AnswersList answerList={answerList} />
						</div>
					)}
					<div className="relationship__body">
						<p>Relaciona arrastrando y soltando</p>

						<div className="relationship__columns">
							{rowContent.map(content => {
								return (
									<ContentRow
										key={content.rowId}
										rowId={content.rowId}
										columns={content.columns}
										sitting={this.props.activity.sitting}
									/>
								);
							})}
						</div>
					</div>
				</DragDropContext>

				{!activity.sitting && (
					<div className="relationship__button">
						<Button disabled={!related} onClick={this.sendResponse}>
							Responder
						</Button>
					</div>
				)}
			</section>
		);
	}
}

export default Relationship;

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Line } from 'rc-progress';

import Frame from '../Frame';
import ActivityList from './ActivityList';
import Button from 'core/components/Button';
import * as activitiesActions from 'core/redux/activities/operations';

// Activity types
import Selection from './Selection';
import Dictation from './Dictation';
import Relationship from './Relationship';
import Area from './Area';

class Activity extends Component {
	constructor (props) {
		super(props);
		this.state = {
			activitySection: 'list',
			currentActivity: {},
			activitiesDone: []
		};
	}

	componentDidMount () {
		this.setState({ currentActivity: this.props.relationships });
	}

	goToSection = (section, activity) => {
		this.setState({ activitySection: section, currentActivity: activity });
	};

	saveActivity = activity => {
		this.setState(prevState => ({
			...prevState,
			activitiesDone: [...prevState.activitiesDone, activity]
		}));
	};

	render () {
		const { activitySection, currentActivity, activitiesDone } = this.state;
		const { mc_questions, dictations, relationships, updateActivity } = this.props;

		const allActivities = [...mc_questions, ...dictations, ...relationships];

		return (
			<div className="activity">
				<Frame
					headerText={
						activitySection === 'list' ? `Actividades: ${allActivities.length}` : currentActivity.title
					}
					activitySection={activitySection}
					goToSection={this.goToSection}
					withGoBack
				>
					{activitySection === 'list' && (
						<div>
							<ActivityList
								mc_questions={mc_questions}
								dictations={dictations}
								relationships={relationships}
								goToSection={this.goToSection}
								activitiesDone={activitiesDone}
							/>

							<div className="activity__footer">
								<div className="activity-progress">
									<div>
										<div className="v-align space-between">
											<div>Progreso</div>
											<div>{activitiesDone.length * 20}%</div>
										</div>
										<Line
											percent={activitiesDone.length * 20}
											strokeWidth="1"
											trailWidth="1"
											strokeColor="#5e43c0"
										/>
									</div>
								</div>
								{activitiesDone.length < 5 ? (
									<Button disabled>Presentar Evaluación</Button>
								) : (
									<Link to="/courses/exam">
										<Button>Presentar Evaluación</Button>
									</Link>
								)}
							</div>
						</div>
					)}

					{activitySection === 'selection' && (
						<Selection
							activity={currentActivity}
							activitiesDone={activitiesDone}
							saveActivity={this.saveActivity}
							goToSection={this.goToSection}
							respondSelection={this.props.respondSelection}
							updateActivity={updateActivity}
						/>
					)}

					{activitySection === 'dictation' && (
						<Dictation
							activity={currentActivity}
							goToSection={this.goToSection}
							getFullDisplayDictation={this.props.getFullDisplayDictation}
							updateActivity={updateActivity}
							respondDictation={this.props.respondDictation}
						/>
					)}

					{activitySection === 'relationship' && (
						<Relationship
							activity={currentActivity}
							respondRelationship={this.props.respondRelationship}
							goToSection={this.goToSection}
							updateActivity={updateActivity}
						/>
					)}

					{activitySection === 'area' && <Area goToSection={this.goToSection} />}
				</Frame>
			</div>
		);
	}
}

export default connect(
	null,
	{
		respondSelection: activitiesActions.respondSelection,
		getFullDisplayDictation: activitiesActions.getFullDisplayDictation,
		respondDictation: activitiesActions.respondDictation,
		respondRelationship: activitiesActions.respondRelationship
	}
)(Activity);

import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Frame extends Component {
	constructor (props) {
		super(props);
		this.state = {};
	}
	render () {
		const { headerText, children, withGoBack, activitySection, goToSection } = this.props;

		return (
			<div className="frame">
				<div className="frame__header">
					{withGoBack
						&& activitySection !== 'list' && (
							<div className="activity__goBack v-align" onClick={() => goToSection('list')}>
								<i className="mdi mdi-arrow-left" />
								<span>Volver</span>
							</div>
						)}

					{headerText}
				</div>
				{children}
			</div>
		);
	}
}

Frame.propTypes = {
	headerText: PropTypes.string
};

export default Frame;

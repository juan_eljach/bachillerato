import React, { Component } from 'react';
import classnames from 'classnames';
import ReactHtmlParser from 'react-html-parser';
import ReactAudioPlayer from 'react-audio-player';

import Frame from '../Frame';
import PropTypes from 'prop-types';

class Multimedia extends Component {
	constructor (props) {
		super(props);
		this.state = {
			selectedMedia: {
				mediaText: '',
				mediaType: ''
			}
		};
	}

	componentDidMount () {
		this.setState({
			selectedMedia: {
				slug: this.props.videos[0] && this.props.videos[0].slug,
				mediaText: this.props.videos[0] && this.props.videos[0].title,
				mediaType: 'video',
				content: this.props.videos[0] && this.props.videos[0].link
			}
		});
	}

	onMultimediaChange = (slug, type, text, content) => {
		this.setState(prevState => {
			return {
				...prevState,
				selectedMedia: {
					...prevState.selectedMedia,
					slug,
					mediaType: type,
					mediaText: text,
					content
				}
			};
		});
	};

	render () {
		const { selectedMedia } = this.state;
		const { videos, texts, documents, audios } = this.props;

		if (!selectedMedia.mediaType) return null;

		return (
			<div className="multimedia">
				<Frame headerText={selectedMedia.mediaText}>
					<div className="player">
						{selectedMedia.mediaType === 'video' && (
							<iframe src={`${selectedMedia.content}?rel=0&controls=1&showinfo=0`} frameBorder="0" />
						)}

						{selectedMedia.mediaType === 'document' && (
							<iframe src={selectedMedia.content} frameBorder="0" />
						)}

						{selectedMedia.mediaType === 'text' && (
							<div className="multimedia__text">{ReactHtmlParser(selectedMedia.content)}</div>
						)}

						{selectedMedia.mediaType === 'audio' && (
							<div className="multimedia__audio">
								<ReactAudioPlayer src={selectedMedia.content} controls />
							</div>
						)}
					</div>

					<div className="multimedia__material">
						<h5>Material del módulo:</h5>

						<div className="multimedia__material-list">
							{videos.map(video => {
								const mediaClassName = classnames('multimedia__material-item v-align', {
									'multimedia__material-item--active': selectedMedia.slug === video.slug
								});

								return (
									<div
										key={video.id}
										className={mediaClassName}
										onClick={event => {
											event.preventDefault();

											this.onMultimediaChange(video.slug, 'video', video.title, video.link);
										}}
									>
										<i className="mdi mdi-video" />
										<div>{video.title}</div>
									</div>
								);
							})}

							{documents.map(doc => {
								const mediaClassName = classnames('multimedia__material-item v-align', {
									'multimedia__material-item--active': selectedMedia.slug === doc.slug
								});

								return (
									<div
										key={doc.id}
										className={mediaClassName}
										onClick={event => {
											event.preventDefault();

											this.onMultimediaChange(
												doc.slug,
												'document',
												doc.slug,
												doc.document || doc.link
											);
										}}
									>
										<i className="mdi mdi-file-document" />
										<div>{doc.title}</div>
									</div>
								);
							})}

							{texts.map(text => {
								const mediaClassName = classnames(
									'multimedia__material-item v-align'
									// {'multimedia__material-item--active': selectedMedia.id === video.id}
								);

								return (
									<div
										key={text.id}
										className={mediaClassName}
										onClick={event => {
											event.preventDefault();

											this.onMultimediaChange(text.id, 'text', text.title, text.content);
										}}
									>
										<i className="mdi mdi-tooltip-text" />
										<div>{text.title}</div>
									</div>
								);
							})}

							{audios.map(audio => {
								return (
									<div
										key={audio.id}
										onClick={event => {
											event.preventDefault();

											this.onMultimediaChange(audio.id, 'audio', audio.title, audio.file);
										}}
										className="multimedia__material-item v-align"
									>
										<i className="mdi mdi-volume-high" />
										<div>{audio.title}</div>
									</div>
								);
							})}
						</div>
					</div>
				</Frame>
			</div>
		);
	}
}

Multimedia.defaultProps = {};

Multimedia.propTypes = {
	videos: PropTypes.array,
	documents: PropTypes.array,
	texts: PropTypes.array,
	audios: PropTypes.array
};

export default Multimedia;

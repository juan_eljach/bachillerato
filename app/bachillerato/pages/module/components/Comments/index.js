import React, { Component } from 'react';

import Frame from '../Frame';
import InputField from 'core/components/InputField';
import Button from 'core/components/Button';

class Comments extends Component {
  constructor (props) {
    super(props);
    this.state = {
      inputFocus: ''
    };
  }

  onFocusInput = (input) => {
    this.setState({inputFocus: input});
  }

  render () {
    const { inputFocus } = this.state;

    return (
      <div className="comments">
        <Frame headerText="¿Dudas? Pide ayuda a un experto">
          <div className="comments__container">
            <div className="comments__comment-input">
              <InputField
                type="textarea"
                onFocus={() => this.onFocusInput('comment')}
                onBlur={() => this.onFocusInput('')}
              />

              {
                inputFocus === 'comment' && (
                  <Button>Enviar</Button>
                )
              }
            </div>

            <div className="comments__comment">
              <div className="comments__comment-user v-align">
                <img src="https://pbs.twimg.com/profile_images/712466126450483200/wmkqffYN_400x400.jpg" alt="" />
                <div className="comments__comment-row v-align space-between">
                  <div className="comments__comment-user-info">
                    <span>Admin</span>
                    <p>Esto es un Comentario</p>
                  </div>

                  <div className="comments__comment-date">31 de Agosto 2018</div>
                </div>
              </div>

              {
                inputFocus === 'answer' ? (
                  <div className="comments__comment-input">
                    <InputField type="textarea" onBlur={() => this.onFocusInput('')} />
                    <Button>Enviar</Button>
                  </div>
                ) : (
                  <div className="comments__comment-respond v-align" onClick={() => this.onFocusInput('answer')}>
                    <i className="mdi mdi-reply"></i>
                    <span>Responder</span>
                  </div>
                )
              }

              <div className="comments__comment-answers">
                <div className="comments__comment-answer">
                  <div className="comments__comment-user v-align">
                    <img src="https://pbs.twimg.com/profile_images/712466126450483200/wmkqffYN_400x400.jpg" alt="" />
                    <div className="comments__comment-row v-align space-between">
                      <div className="comments__comment-user-info">
                        <span>User1</span>
                        <p>Esto es una Respuesta</p>
                      </div>

                      <div className="comments__comment-date">Hoy</div>
                    </div>
                  </div>
                </div>

                <div className="comments__comment-answer">
                  <div className="comments__comment-user v-align">
                    <img src="https://pbs.twimg.com/profile_images/712466126450483200/wmkqffYN_400x400.jpg" alt="" />
                    <div className="comments__comment-row v-align space-between">
                      <div className="comments__comment-user-info">
                        <span>User2</span>
                        <p>Esto es una Respuesta</p>
                      </div>

                      <div className="comments__comment-date">Hoy</div>
                    </div>
                  </div>
                </div>

                <div className="comments__comment-answer-see">Ver todas las respuestas</div>
              </div>
            </div>
          </div>
        </Frame>
      </div>
    );
  }
}

export default Comments;

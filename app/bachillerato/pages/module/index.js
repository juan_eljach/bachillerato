import React, { Component } from 'react';
import { connect } from 'react-redux';

import Multimedia from './components/Multimedia';
import Activities from './components/Activities';
import Comments from './components/Comments';
import Loading from 'core/components/Loading';
import NoData from 'core/components/NoData';

import * as modulesActions from 'core/redux/modules/operations';
import * as subjectActions from 'core/redux/subjects/operations';
import * as coursesActions from 'core/redux/courses/operations';

class Module extends Component {
	constructor (props) {
		super(props);
		this.state = {
			course: {},
			subject: {},
			module: {
				videos: [],
				documents: [],
				texts: [],
				audios: []
			},
			isFetching: false,
			pageLoaded: false
		};

		this.params = this.props.match.params;
	}

	async componentDidMount () {
		this.setState({ isFetching: true });

		const course = await this.props.getCourse(this.params.courseSlug);
		const subject = await this.props.getSubject(this.params.courseSlug, this.params.subjectSlug);

		const module = await this.props.getModule(
			this.params.courseSlug,
			this.params.subjectSlug,
			this.params.moduleSlug
		);

		this.setState({ course, subject, module, pageLoaded: true, isFetching: false });
	}

	updateActivity = (type, data) => {
		this.setState(prevState => {
			return {
				...prevState,
				module: {
					...prevState.module,
					[type]: prevState.module[type].map(a => {
						if (a.id === data.id) {
							return { ...a, ...data };
						}

						return a;
					})
				}
			};
		});
	};

	render () {
		const { course, subject, module, isFetching, pageLoaded } = this.state;

		if (isFetching && !pageLoaded) return <Loading type="loading" />;
		console.log(module);
		return (
			<div className="module wrap-container">
				<div className="module__header">
					<h2>{module.title}</h2>
					<p>
						{subject.name} - {course.name}
					</p>
				</div>

				<div className="module__body">
					<Multimedia
						videos={module.videos}
						documents={module.documents}
						texts={module.texts}
						audios={module.audios}
					/>

					<div>
						{module.mc_questions && (
							<Activities
								mc_questions={module.mc_questions}
								dictations={module.dictations}
								relationships={module.relationships}
								updateActivity={this.updateActivity}
							/>
						)}
					</div>
				</div>
			</div>
		);
	}
}

export default connect(
	null,
	{
		getModule: modulesActions.getModule,
		getSubject: subjectActions.getSubject,
		getCourse: coursesActions.getCourse
	}
)(Module);

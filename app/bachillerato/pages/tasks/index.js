import React, { Component } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';

import Loading from 'core/components/Loading';
import Answer from 'core/components/Answer';
import Button from 'core/components/Button';
import NoData from 'core/components/NoData';

import * as tasksOperations from 'core/redux/tasks/operations';

class Tasks extends Component {
	constructor (props) {
		super(props);
		this.state = {
			tasks: [],
			selectedTask: {
				answers: []
			},
			selectedAnswer: {},
			answered: [],
			isFetching: false,
			pageLoaded: false,
			isSending: false
		};
	}

	async componentDidMount () {
		this.setState({ isFetching: true });

		const response = await this.props.getTasks(this.props.match.params.courseSlug);

		console.log(response);

		this.setState({ tasks: response, selectedTask: response[0], isFetching: false, pageLoaded: true });
	}

	onSelectTask = item => {
		this.setState({ selectedTask: item });
	};

	onSelectAnswer = answer => {
		this.setState({ selectedAnswer: answer });
	};

	respond = async () => {
		const { selectedTask, selectedAnswer } = this.state;

		this.setState({ isSending: true });

		let payload = {
			student: this.props.user.id,
			mc_question: selectedTask.id,
			given_answer: selectedAnswer.id
		};

		const response = await this.props.sendTask(payload);
		console.log(response);
		this.setState(prevState => {
			return {
				...prevState,
				selectedTask: {
					...prevState.selectedTask,
					sitting: response
				},
				tasks: prevState.tasks.map(task => {
					if (task.id === response.id) {
						return { ...task, sitting: response };
					}

					return task;
				}),
				isSending: false
			};
		});
	};

	render () {
		const { isFetching, isSending, pageLoaded, tasks, selectedTask, selectedAnswer, answered } = this.state;

		if (isFetching && !pageLoaded) return <Loading type="loading" />;

		if (!tasks.length) return <NoData text={'No hay tareas disponibles'} />;

		return (
			<section className="tasks wrap-container">
				<div className="tasks-list">
					<h5>Lista de Tareas:</h5>
					<ul>
						{tasks.map(task => {
							const itemClassName = classnames('tasks-list__item v-align', {
								'tasks-list__item--active': selectedTask.id === task.id
							});

							return (
								<li key={task.id} className={itemClassName} onClick={() => this.onSelectTask(task)}>
									{!task.sitting ? (
										<i className="mdi mdi-checkbox-blank-circle-outline" />
									) : (
										<i className="mdi mdi-check-circle" />
									)}

									<div>
										<div>{task.title}</div>
										<span>26 sept. 2018</span>
									</div>
								</li>
							);
						})}
					</ul>
				</div>

				<div className="tasks-content">
					<div className="tasks-content__text">
						<p>{selectedTask.content}</p>
					</div>

					<div className="tasks-content__question">
						<p>{selectedTask.explanation}</p>
					</div>

					<div>
						{selectedTask.answers.map(answer => {
							const isAnswered = answered.find(a => a.task === selectedTask.value);
							const { sitting } = selectedTask;

							return (
								<Answer
									key={answer.id}
									content={answer.content}
									selected={answer.id === selectedAnswer.id}
									sitting={!!sitting}
									isGivenAnswer={sitting && answer.id === sitting.given_answer}
									isRightAnswer={sitting && sitting.is_right_answer}
									isCorrect={sitting && answer.correct}
									onClick={() => this.onSelectAnswer(answer)}
								/>
							);
						})}
					</div>

					{!selectedTask.sitting && (
						<div className="tasks-content__buttons">
							<Button isFetching={isSending} onClick={this.respond}>
								Enviar
							</Button>
						</div>
					)}
				</div>
			</section>
		);
	}
}

const mapStateToProps = ({ user }) => {
	return {
		user
	};
};

export default connect(
	mapStateToProps,
	{
		getTasks: tasksOperations.getTasks,
		sendTask: tasksOperations.sendTask
	}
)(Tasks);

import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import InputField from 'core/components/InputField';
import Tooltip from 'core/components/Tooltip';

class Navbar extends Component {
	constructor (props) {
		super(props);
	}

	render () {
		const { courses, selectedCourse, onChangeCourse, onToggleRequirements } = this.props;

		return (
			<nav className="user-navbar">
				<div className="wrap-container">
					{!!courses.length && (
						<div className="user-navbar__courses">
							<InputField
								type="select"
								value={selectedCourse}
								placeholder="Seleccionar ciclo"
								options={courses}
								onChange={onChangeCourse}
							/>
						</div>
					)}

					<ul className="user-navbar__menu v-align">
						<li>
							<Link to={`/tasks/${selectedCourse.slug}`}>
								<i className="mdi mdi-check-circle" />
								<span>Tareas</span>
							</Link>
						</li>
						<li>
							<Tooltip title="Requisitos enviados correctamente">
								<a onClick={onToggleRequirements}>
									{true ? (
										<i className="mdi mdi-account-check" />
									) : (
										<i className="mdi mdi-alert-circle-outline" />
									)}
									<span>Requisitos</span>
								</a>
							</Tooltip>
						</li>
						<li>
							<Link to="/profile?tab=payments">
								<i className="mdi mdi-currency-usd" />
								<span>Mis Pagos</span>
							</Link>
						</li>
						<li>
							<Link to="/referrals">
								<i className="mdi mdi-account-multiple" />
								<span>Mis Referidos</span>
							</Link>
						</li>
					</ul>
				</div>
			</nav>
		);
	}
}

export default Navbar;

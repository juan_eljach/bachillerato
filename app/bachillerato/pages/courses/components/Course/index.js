import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Line } from 'rc-progress';

import Button from 'core/components/Button';
import Pagination from 'core/components/Pagination';
import Tooltip from 'core/components/Tooltip';

class Course extends Component {
	constructor (props) {
		super(props);
	}

	render () {
		const { subject } = this.props;

		return (
			<div className="course">
				<div className="course__header v-align course__row">
					<div className="course__header-title">{subject.name}</div>
					<div className="course__header-note">
						<div>Nota</div>
						<div>{subject.current_grade}</div>
					</div>
					<div className="course__progress">
						<span>{subject.percentage_completed}%</span>
						<Line
							percent={subject.percentage_completed}
							strokeWidth="3"
							trailWidth="3"
							strokeColor="#5e43c0"
						/>
					</div>
				</div>

				{subject.modules.map((module, index) => (
					<div className="course__module v-align course__row" key={module.id}>
						<div className="v-align">
							<div className="course__module-number">
								<span>{index + 1}</span>
							</div>

							<p className="course__module-title">
								<Link
									to={`/courses/${subject.courseSlug}/subject/${subject.slug}/module/${module.slug}`}
								>
									{module.title}
								</Link>
							</p>
						</div>

						<div className="course__module-note">
							<div>Nota</div>
							<div>{module.current_grade || 0}</div>
						</div>
						<div className="course__progress">
							<span>{module.percentage_completed || 0}%</span>
							<Line
								percent={module.percentage_completed || 0}
								strokeWidth="3"
								trailWidth="3"
								strokeColor="#5e43c0"
							/>
						</div>
					</div>
				))}

				<div className="course__footer">
					<Tooltip
						disabled={!!subject.subjectExam}
						title="El examen estará deshabilitado hasta que finalices todos los módulos"
					>
						{subject.subjectExam ? (
							<Link to={`/courses/exam/${subject.subjectExam}`}>
								<Button>Presentar Examen Final</Button>
							</Link>
						) : (
							<Button disabled>Presentar Examen Final</Button>
						)}
					</Tooltip>

					<Pagination current={1} totalItems={18} perPage={6} />
				</div>
			</div>
		);
	}
}

export default Course;

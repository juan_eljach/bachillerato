import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';

import Navbar from './components/Navbar';
import Course from './components/Course';
import Modal from 'core/components/Modal';
import Tooltip from 'core/components/Tooltip';
import Loading from 'core/components/Loading';
import NoData from 'core/components/NoData';
import * as coursesOperations from 'core/redux/courses/operations';
import * as coursesActions from 'core/redux/courses/actions';
import * as subjectsActions from 'core/redux/subjects/operations';
import * as modulesActions from 'core/redux/modules/operations';
import * as userActions from 'core/redux/user/operations';

class Courses extends Component {
	constructor (props) {
		super(props);
		this.state = {
			courses: [],
			subjects: [],
			documents: [],
			selectedCourse: {},
			requirementsOpen: false,
			requirementsDone: true,
			isFetching: false,
			pageLoaded: false
		};
	}

	async componentDidMount () {
		try {
			this.setState({ isFetching: true });

			const courses = await this.props.getCourses();

			if (!courses.length) {
				return this.setState({
					isFetching: false,
					pageLoaded: true
				});
			}

			const currentCourse = courses[0];

			this.props.setCourse(currentCourse);

			const subjects = await this.getSubjectsWithModules(currentCourse.slug);

			const documents = await this.props.getDocuments();

			this.setState(prevState => {
				return {
					...prevState,
					courses: courses.map(course => {
						return { ...course, value: course.id, label: course.title };
					}),
					subjects,
					documents,
					selectedCourse: { ...currentCourse, label: currentCourse.title, value: currentCourse.id },
					isFetching: false,
					pageLoaded: true
				};
			});
		} catch (error) {
			//error
		}
	}

	onChangeCourse = async value => {
		const subjects = await this.getSubjectsWithModules(value.slug);

		this.setState(prevState => {
			if (value.id !== prevState.selectedCourse.id) {
				return {
					...prevState,
					selectedCourse: value,
					subjects
				};
			}

			return {
				...prevState,
				subjects
			};
		});
	};

	/**
	 * Returns the subjects with their corresponding modules
	 * @param {string} courseSlug - The slug of the current course
	 * @returns {Promise} Promise object reperesents the subjects
	 */
	getSubjectsWithModules = async courseSlug => {
		const subjects = await this.props.getSubjects(courseSlug);

		return await Promise.all(
			subjects.map(async subject => {
				const modules = await this.props.getModules(courseSlug, subject.slug);
				const subjectDetail = await this.props.getSubject(courseSlug, subject.slug);

				return { ...subject, modules, courseSlug, subjectExam: subjectDetail.subject_exam };
			})
		);
	};

	onToggleRequirements = () => {
		this.setState({ requirementsOpen: !this.state.requirementsOpen });
	};

	render () {
		const {
			pageLoaded,
			courses,
			selectedCourse,
			subjects,
			documents,
			requirementsOpen,
			requirementsDone,
			isFetching
		} = this.state;

		if (isFetching && !pageLoaded) return <Loading type="loading" />;

		return (
			<Fragment>
				<Navbar
					courses={courses}
					selectedCourse={selectedCourse}
					onChangeCourse={this.onChangeCourse}
					onToggleRequirements={this.onToggleRequirements}
				/>

				<section className="courses wrap-container">
					{!courses.length && (
						<NoData text={`No hay ${!subjects.length ? 'materias' : 'cursos'} disponibles`} />
					)}

					<div className="courses__list">
						{subjects.map(subject => {
							return <Course subject={subject} key={subject.id} />;
						})}
					</div>
				</section>

				<Modal open={requirementsOpen}>
					<div className="requirements">
						<div className="requirements__header v-align space-between">
							<h5>
								<span>Requisitos</span>
								<Tooltip title="Requisitos necesarios para finalizar el proceso de estudio.">
									<i className="mdi mdi-help-circle" />
								</Tooltip>
							</h5>
							<i className="mdi mdi-close action-hover" onClick={this.onToggleRequirements} />
						</div>

						<ul className="requirements__list">
							{documents.map(doc => {
								return (
									<li className="v-align">
										{requirementsDone ? (
											<i className="mdi mdi-check" />
										) : (
											<i className="mdi mdi-alert-circle-outline" />
										)}
										<span>Certificado</span>
									</li>
								);
							})}
						</ul>
					</div>
				</Modal>
			</Fragment>
		);
	}
}

export default connect(
	null,
	{
		getCourses: coursesOperations.getCourses,
		getSubjects: subjectsActions.getSubjects,
		getSubject: subjectsActions.getSubject,
		getModules: modulesActions.getModules,
		getDocuments: userActions.getDocuments,
		setCourse: coursesActions.setCourse
	}
)(Courses);

import React, { Component } from 'react';

class UserActivity extends Component {
  constructor (props) {
    super(props);
  }

  render () {
    return (
      <div className="user-activity">
        <div className="user-activity__list">
          <div className="user-activity__item v-align">
            <img src="https://pbs.twimg.com/profile_images/712466126450483200/wmkqffYN_400x400.jpg" alt="" />
            <div className="user-activity__item-info">
              <div className="user-activity__item-name v-align">
                <div>John Doe</div>
                <span>(hace 5 minutos)</span>
              </div>

              <div className="user-activity__item-description">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt, blanditiis. Quod assumenda nobis corrupti provident error aut dolore incidunt.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default UserActivity;

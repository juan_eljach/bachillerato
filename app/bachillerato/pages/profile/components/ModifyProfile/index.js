import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import moment from 'moment';

import InputField from 'core/components/InputField';
import Button from 'core/components/Button';

class ModifyProfile extends Component {
	constructor (props) {
		super(props);
		this.state = {
			isSaving: false
		};
	}

	componentDidMount = () => {};

	static validate (fields) {
		const errors = {};
		const errorName = 'Campo requerido';

		if (!fields.first_name) errors.first_name = errorName;
		if (!fields.last_name) errors.last_name = errorName;
		if (!fields.cellphone) errors.cellphone = errorName;
		if (!fields.birthday) errors.birthday = errorName;

		return errors;
	}

	onSubmit = async values => {
		this.setState({ isSaving: true });

		let payload = {
			cellphone: values.cellphone
		};
		const response = await this.props.updateProfile(payload);

		this.setState({ isSaving: false });
	};

	render () {
		const { isSaving } = this.state;
		const { handleSubmit } = this.props;

		return (
			<form className="modify-profile" onSubmit={handleSubmit(this.onSubmit)}>
				<Field type="text" name="first_name" label="Nombre" component={InputField} disabled />

				<Field type="text" name="last_name" label="Apellidos" component={InputField} disabled />

				<Field type="text" name="cellphone" label="Celular" component={InputField} />

				<Field
					type="select"
					name="city"
					label="Ciudad"
					placeholder="Seleccionar"
					component={InputField}
					options={[]}
					onChange={() => {}}
				/>

				<Field type="text" name="neighborhood" label="Barrio" component={InputField} />

				<Field type="text" name="address" label="Dirección" component={InputField} />

				<div className="h-align">
					<Button isFetching={isSaving}>MODIFICAR</Button>
				</div>
			</form>
		);
	}
}

export default reduxForm({
	form: 'ModifyProfile',
	validate: ModifyProfile.validate,
	keepDirtyOnReinitialize: true,
	enableReinitialize: true
})(ModifyProfile);

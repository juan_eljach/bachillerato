import React, { Component } from 'react';
import moment from 'moment';

import Table, { TableHead, TableBody, TableRow, TableCell } from 'core/components/Table';

class Payments extends Component {
  constructor (props) {
    super(props);
    this.state = {};
  }
  render () {
    const { payments } = this.props;

    return (
      <div className="payments">
        <Table
          type="data-grid"
        >
          <TableHead>
            <TableRow>
              <TableCell>Método de pago</TableCell>
              <TableCell>Valor pagado</TableCell>
              <TableCell>Fecha de pago</TableCell>
              <TableCell>Materia</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {
              payments.map((payment) => {
                return (
                  <TableRow key={payment.id}>
                    <TableCell>{payment.payment_method}</TableCell>
                    <TableCell>{payment.paid_price}</TableCell>
                    <TableCell>{moment(payment.payment_date).format('DD MMM YYYY')}</TableCell>
                    <TableCell>{payment.subject}</TableCell>
                  </TableRow>
                );
              })
            }
          </TableBody>
        </Table>
      </div>
    );
  }
}

export default Payments;

import React, { Component } from 'react';
import { Line } from 'rc-progress';

class UserProfile extends Component {
  constructor (props) {
    super(props);
  }

  render () {
    const { user } = this.props;

    return (
      <div className="profile-info-user">
        <div className="profile-info-user__data-list">
          <div className="profile-info-user__data-item">
            <label>Nombre completo</label>
            <div>{`${user.first_name} ${user.last_name}`}</div>
          </div>

          <div className="profile-info-user__data-item">
            <label>Teléfono</label>
            <div>{user.cellphone}</div>
          </div>

          <div className="profile-info-user__data-item">
            <label>Correo</label>
            <div>e@gmail.com</div>
          </div>

          <div className="profile-info-user__data-item">
            <label>País</label>
            <div>Colombia</div>
          </div>
        </div>

        <div className="profile-info-user__description">
        </div>

        <div className="profile-info-user__advanced">
          <h5>Notas de las materias</h5>
          <ul>
            <li>
              <div>Biología</div>
              <span>10%</span>
              <Line percent={10} strokeWidth="1" trailWidth="1" strokeColor="#5e43c0" />
            </li>
            <li>
              <div>Matemáticas</div>
              <span>30%</span>
              <Line percent={30} strokeWidth="1" trailWidth="1" strokeColor="#5e43c0" />
            </li>
            <li>
              <div>Español</div>
              <span>70%</span>
              <Line percent={70} strokeWidth="1" trailWidth="1" strokeColor="#5e43c0" />
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

export default UserProfile;

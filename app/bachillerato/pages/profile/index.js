import React, { Component } from 'react';
import { connect } from 'react-redux';
import queryString from 'query-string';
import classnames from 'classnames';

import UserProfile from './components/UserProfile';
import UserActivity from './components/UserActivity';
import ModifyProfile from './components/ModifyProfile';
import Payments from './components/Payments';
import Loading from 'core/components/Loading';
import * as userActions from 'core/redux/user/operations';

class Profile extends Component {
	constructor (props) {
		super(props);

		this.state = {
			selectedTab: 'profile',
			uploadingImage: false
		};

		this.tabs = [
			{ value: 'profile', label: 'Perfil' },
			// { value: 'activity', label: 'Actividad' },
			{ value: 'payments', label: 'Pagos' },
			{ value: 'modify', label: 'Modificar Perfil' }
		];
	}

	componentDidMount () {
		const selectedTab = queryString.parse(this.props.location.search).tab;

		if (selectedTab) {
			this.setState({ selectedTab });
		}
	}

	onImageChange = async event => {
		this.setState({ uploadingImage: true });

		let formData = new window.FormData(this.newTicketForm);

		formData.append('image', event.target.files[0]);

		await this.props.updateProfile(formData);

		this.setState({ uploadingImage: false });
	};

	onTabChange = tab => {
		this.setState({ selectedTab: tab });
	};

	render () {
		const { selectedTab, uploadingImage } = this.state;
		const { user, updateProfile } = this.props;

		if (!user.id) return null;

		return (
			<section className="profile wrap-container">
				<div className="profile-short-info">
					<div className="profile-short-info__header h-align v-align">
						<div>
							<img src={user.image ? user.image : '/static/app/img/user_avatar.png'} />
							<div>user@gmail.com</div>
							{uploadingImage ? (
								<Loading type="circles" />
							) : (
								<label htmlFor="profilePicture" className="profile-short-info__edit v-align h-align">
									<i className="mdi mdi-pencil" />
									<span>Editar</span>
									<input type="file" id="profilePicture" onChange={this.onImageChange} />
								</label>
							)}
						</div>
					</div>

					<div className="profile-short-info__extra">
						<p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>

						<div className="profile-short-info__more v-align h-align">
							<div className="v-align">
								<i className="mdi mdi-account-location" />
								<span>Sexto</span>
							</div>
							<div className="v-align">
								<i className="mdi mdi-account-star" />
								<span>Estudiante</span>
							</div>
						</div>
					</div>
				</div>

				<div className="profile-info">
					<div className="profile-info__tab v-align">
						{this.tabs.map((tab, index) => {
							const tabItemClassName = classnames('profile-info__tab-item', {
								'profile-info__tab-item--active': tab.value === selectedTab
							});

							return (
								<div
									key={index}
									className={tabItemClassName}
									onClick={() => this.onTabChange(tab.value)}
								>
									{tab.label}
								</div>
							);
						})}
					</div>

					<div className="profile-info__main">
						{selectedTab === 'profile' && <UserProfile user={user} />}

						{selectedTab === 'activity' && <UserActivity />}

						{selectedTab === 'modify' && (
							<ModifyProfile initialValues={user} updateProfile={updateProfile} />
						)}

						{selectedTab === 'payments' && <Payments payments={user.enrollments} />}
					</div>
				</div>
			</section>
		);
	}
}

const mapStateToProps = ({ user }) => {
	return {
		user
	};
};

export default connect(
	mapStateToProps,
	{
		updateProfile: userActions.updateProfile
	}
)(Profile);

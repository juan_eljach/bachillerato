import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

import Button from 'core/components/Button';
import Pagination from 'core/components/Pagination';
import Loading from 'core/components/Loading';
import * as examsActions from 'core/redux/exams/operations';

class Exam extends Component {
	constructor (props) {
		super(props);
		this.state = {
			exam: {},
			sitting: null,
			selectedAnswer: null,
			currentQuestion: { answers: [] },
			questions: {},
			page: 1,
			pageLoaded: false,
			isSending: false
		};
	}

	async componentDidMount () {
		if (this.props.match.params) {
			const exam = await this.props.getExam(this.props.match.params.examId);

			if (exam.sitting && exam.sitting.answered_questions.length === exam.questions.length) {
				this.props.history.goBack();
			}

			const questions = exam.questions.reduce((acc, current, index) => {
				const currentNumber = index + 1;
				return { ...acc, [currentNumber]: { ...current, currentNumber } };
			}, {});

			this.setState({ questions, currentQuestion: questions[1], exam, sitting: exam.sitting, pageLoaded: true });
		}
	}

	onSelectAnswer = answer => {
		if (this.state.sitting && this.state.sitting.answered_questions.length === this.state.exam.questions.length) {
			return;
		}

		this.setState({ selectedAnswer: answer });
	};

	onQuestionChange = page => {
		this.setState({ page, currentQuestion: this.state.questions[page], selectedAnswer: '' });
	};

	onSumbmitAnswer = async () => {
		try {
			const { currentQuestion, selectedAnswer, exam, page } = this.state;

			this.setState({ isSending: true });

			let payload = {
				question: currentQuestion.id,
				answer: selectedAnswer
			};

			const response = await this.props.checkAnswer(exam.id, payload);
			if (response.completed) {
				this.setState(prevState => {
					return {
						...prevState,
						exam: {
							...prevState.exam,
							sitting: { ...prevState.exam.sitting, ...response }
						},
						sitting: {
							...prevState.sitting,
							...response
						},
						completed: true,
						isSending: false
					};
				});

				return;
			}

			this.setState(prevState => {
				const nextPage = page + 1;

				let newState = {
					...prevState,
					questions: {
						...prevState.questions,
						[page]: { ...prevState.questions[page], is_correct: response.is_correct }
					},
					currentQuestion: prevState.questions[nextPage],
					page: nextPage,
					selectedAnswer: null,
					isSending: false
				};

				if (prevState.sitting) {
					newState.sitting.answered_questions = [...prevState.sitting.answered_questions, currentQuestion.id];

					if (response.is_correct) {
						newState.sitting.right_answered_questions = [
							...prevState.sitting.right_answered_questions,
							currentQuestion.id
						];
					} else {
						newState.sitting.wrong_answered_questions = [
							...prevState.sitting.wrong_answered_questions,
							currentQuestion.id
						];
					}
				} else {
					newState.sitting = { answered_questions: [currentQuestion.id] };

					if (response.is_correct) {
						newState.sitting = {
							...newState.sitting,
							right_answered_questions: [currentQuestion.id]
						};
					} else {
						newState.sitting = {
							...newState.sitting,
							wrong_answered_questions: [currentQuestion.id]
						};
					}
				}

				return newState;
			});
		} catch (error) {
			console.log(error);
		}
	};

	render () {
		const { pageLoaded, isSending, page, exam, selectedAnswer, currentQuestion, questions, sitting } = this.state;

		if (!pageLoaded) return <Loading type="loading" />;

		return (
			<section className="exam wrap-container">
				<div className="exam__container">
					{sitting && sitting.completed ? (
						<div className="exam__results">
							{sitting.approved ? (
								<div>
									<i className="mdi mdi-check" />
									<h5>!Felicitaciones, aprobaste el examen!</h5>
								</div>
							) : (
								<div>
									<i className="mdi mdi-check" />
									<h5>No aprobaste el examen.</h5>
								</div>
							)}

							<Link to="/">Regresar</Link>
						</div>
					) : (
						<div className="exam__content">
							<div className="exam__header v-align space-between">
								<div className="exam__title">
									<h5>{exam.title}</h5>
								</div>
								<div>
									<Button>
										Pregunta {currentQuestion.currentNumber} de {Object.keys(questions).length}
									</Button>
								</div>
							</div>

							<div className="exam__answer">
								<div className="exam__answer-content">
									<p>{currentQuestion.explanation}</p>

									<div className="exam__answer-answers">
										{currentQuestion.answers.map((answer, index) => {
											const itemClassName = classnames('exam__answer-item', {
												'exam__answer-item--active': answer.id === selectedAnswer
											});

											if (answer.img) {
												return (
													<div
														className={itemClassName}
														key={index}
														onClick={() => this.onSelectAnswer(answer.id)}
													>
														<span>{answer.l}</span>
														<p>
															<img src={answer.img} alt="" />
														</p>
													</div>
												);
											}

											return (
												<div
													className={itemClassName}
													key={answer.id}
													onClick={() => this.onSelectAnswer(answer.id)}
												>
													<span>{index + 1}</span>
													<p className="v-align">{answer.content}</p>
												</div>
											);
										})}
									</div>
								</div>
							</div>

							<div className="exam__submit">
								<Button
									disabled={!selectedAnswer}
									isFetching={isSending}
									onClick={this.onSumbmitAnswer}
								>
									Enviar
								</Button>
							</div>
						</div>
					)}
				</div>
			</section>
		);
	}
}

export default connect(
	null,
	{
		getExam: examsActions.getExam,
		checkAnswer: examsActions.checkAnswer
	}
)(Exam);

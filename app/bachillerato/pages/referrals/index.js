import React, { Component } from 'react';

import Table, { TableHead, TableBody, TableRow, TableCell } from 'core/components/Table';

class Referrals extends Component {
  constructor (props) {
    super(props);
    this.state = {};
  }
  render () {
    return (
      <section className="referrals wrap-container">
        <div className="referrals__image">
          <img src="/static/app/img/referrals.jpeg" alt="" />
        </div>
        <div className="referrals-info">
          <div className="referrals-info__my">
            <h5>Mis Referidos</h5>
            <Table
              type="data-grid"
            >
              <TableHead>
                <TableRow>
                  <TableCell>Código</TableCell>
                  <TableCell>Plan</TableCell>
                  <TableCell>Recibes en bono</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow>
                  <TableCell>54321</TableCell>
                  <TableCell>Plan Mensual</TableCell>
                  <TableCell>
                    <div className="referrals-info__number-round">$10.000</div>
                  </TableCell>
                </TableRow>

                <TableRow>
                  <TableCell>1234</TableCell>
                  <TableCell>Plan Mensual</TableCell>
                  <TableCell>
                    <div className="referrals-info__number-round">$10.000</div>
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </div>
          <div className="referrals-info__about">
            <h5>¿Qué es?</h5>
            <p>
              Es un método diseñado para que nuestros estudiantes pueda obtener grandes beneficios por recomendar
              a sus amigos o familiares a que usen nuestros servicios.
            </p>

            <hr/>

            <h5>¿Cómo funciona?</h5>
            <p>
              Cuando tu amigo haga la matricula <strong>tendrá que poner tu código de estudiante (lo encuentras en la parte superior izquierda de la plataforma) en la
              en la casilla de texto que dirá "código del amigo que te invitó".</strong> <br/>
              Cada vez que nos recomiendes con un amigo y este de inicio a sus estudios, obtendrás bonos cada vez que uno de ellos
              haga uno de estos pagos.
            </p>
          </div>
          <div className="referrals-info__table">
            <h5>Tabla explicativa</h5>

            <Table
              type="data-grid"
            >
              <TableHead>
                <TableRow>
                  <TableCell>Planes</TableCell>
                  <TableCell>Valor</TableCell>
                  <TableCell>Recibes en bono</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow>
                  <TableCell>Plan Mensual</TableCell>
                  <TableCell>$10.000</TableCell>
                  <TableCell>
                    <div className="referrals-info__number-round">$10.000</div>
                  </TableCell>
                </TableRow>

                <TableRow>
                  <TableCell>Plan Mensual</TableCell>
                  <TableCell>$20.000</TableCell>
                  <TableCell>
                    <div className="referrals-info__number-round">$10.000</div>
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </div>
        </div>
      </section>
    );
  }
}

export default Referrals;

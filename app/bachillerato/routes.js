import React, { Component, Fragment } from 'react';
import { Switch, HashRouter, Route } from 'react-router-dom';

// Pages
import Courses from './pages/courses';
import Module from './pages/module';
import Exam from './pages/exam';
import Profile from './pages/profile';
import Tasks from './pages/tasks';
import Messages from './pages/messages';
import Referrals from './pages/referrals';
import Students from './pages/students';
import Complete from './pages/complete';

class AppRouter extends Component {
	constructor (props) {
		super(props);
	}

	render () {
		return (
			<HashRouter basename="/">
				<Fragment>
					{this.props.children}
					<Switch>
						<Route exact path="/" component={Courses} />
						<Route exact path="/module" component={Module} />
						<Route
							exact
							path="/courses/:courseSlug/subject/:subjectSlug/module/:moduleSlug"
							component={Module}
						/>
						<Route exact path="/courses/exam/:examId" component={Exam} />
						<Route exact path="/profile" component={Profile} />
						<Route exact path="/tasks/:courseSlug" component={Tasks} />
						<Route exact path="/referrals" component={Referrals} />
						<Route exact path="/students" component={Students} />
						<Route exact path="/complete" component={Complete} />
					</Switch>
				</Fragment>
			</HashRouter>
		);
	}
}

export default AppRouter;

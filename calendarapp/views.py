from django.shortcuts import render
from rest_framework import generics
from .models import Calendar
from .serializers import CalendarSerializer

class CalendarDetailView(generics.RetrieveAPIView):
    queryset = Calendar.objects.all()
    model_class = Calendar
    serializer_class = CalendarSerializer
    lookup_field = 'course__slug__iexact'
    lookup_url_kwarg = 'course_slug'

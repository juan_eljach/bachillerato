from django.db import models
from courses.models import Course, Subject
from django.template import defaultfilters
from hashlib import sha1

class Calendar(models.Model):
    name = models.CharField(max_length=50)
    course = models.OneToOneField(Course, on_delete=models.CASCADE)

    def __str__(self):
        return ('{0} - {1}'.format(self.name, self.course))

class Event(models.Model):
    time = models.DateTimeField()
    title = models.CharField(max_length=50)
    description = models.TextField()
    calendar = models.ForeignKey(Calendar, on_delete=models.CASCADE, blank=False, null=False, related_name='events')
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)

    def __str__(self):
        return ('{0} - {1}'.format(self.title, self.subject))

# Create your models here.

from django.urls import path
from .views import CalendarDetailView

urlpatterns = [
    path('courses/<slug:course_slug>/calendar/', CalendarDetailView.as_view()),
]

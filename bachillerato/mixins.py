from django.shortcuts import get_object_or_404
from courses.models import Course, Subject

class SlugManagerMixin(object):
    def get_course(self):
        course_slug = self.kwargs.get('course_slug')
        course = get_object_or_404(Course, slug__iexact=course_slug)
        return course

    def get_subject(self):
        subject_slug = self.kwargs.get('subject_slug')
        subject = get_object_or_404(Subject, slug__iexact=subject_slug)
        return subject

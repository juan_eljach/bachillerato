from .base import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'bachillerato_db',
	'USER': 'bachilleratoadmin',
	'PASSWORD': 'bachilleratopassword',
	'HOST': 'localhost',
	'PORT': '5432',
    }
}

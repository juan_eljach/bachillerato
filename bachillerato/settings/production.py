from .base import *

STATIC_ROOT = normpath(join(SITE_ROOT, 'assets'))

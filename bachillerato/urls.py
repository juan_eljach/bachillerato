"""bachillerato URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import include
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('allauth.urls')),
    path('', include('accounts.urls')),
    path('api/', include('courses.urls')),
    path('api/', include('exams.urls')),
    path('api/', include('calendarapp.urls')),
    path('api/', include('messages_app.urls')),
    path('api/', include('documents.urls')),
    path('api/activities/', include('activities.urls')),
    path('api/questions/', include('questions.urls')),
    path('', include('website.urls')),
    path('searchableselect/', include('searchableselect.urls')),
    path('blog/', include('zinnia.urls')),
    path('comments/', include('django_comments.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

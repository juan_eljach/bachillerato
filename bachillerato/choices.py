from django.utils.translation import gettext_lazy as _

LEVEL_OF_STUDIES_CHOICES = (
    ("elementary", _("Elementary School")),
    ("sixth", _("Sixth")),
    ("seventh", _("Seventh")),
    ("eighth", _("Eighth")),
    ("ninth", _("Ninth")),
    ("tenth", _("Tenth")),
    ("eleven", _("Eleven")),
    ("technical", _("Technical")),
    ("technologist", _("Technologist")),
    ("university", _("University")),
)

CIVIL_STATUS_CHOICES = (
    ("single", _("Single")),
    ("engaged", _("Engaged")),
    ("married", _("Married")),
    ("divorced", _("Divorced")),
    ("widowed", _("Widowed"))
)

STUDENT_STATUS = (
    ("signedup", _("Signed up")),
    ("enrolled", _("Enrolled")),
    ("promoted", _("Promoted")),
    ("graduated", _("Graduated"))
)

STUDENT_DOCUMENT_CHOICES = (
    ("photo", _("Photo")),
    ("document", _("Identity Document")),
    ("sixth-cert", _("Sixth Certificate")),
    ("seventh-cert", _("Seventh Certificate")),
    ("eighth-cert", _("Eighth Certificate")),
    ("ninth-cert", _("Ninth Certificate")),
    ("tenth-cert", _("Tenth Certificate")),
)

SCHOOLS_ON_ALLIANCE = (
    ("iberoamericano", "Colegio Iberoamericano"),
    ("ciencia", "Centro Educativo Ciencia"),
    ("jose-acevedo", "Colegio Cooperativo Jose Acevedo y Gomez")
)


CYCLE_GRADE_CHOICES = (
    ('three', 'III'),
    ('four', 'IV'),
    ('five', 'V'),
    ('six', 'VI'),
)

GRADE_CHOICES = (
    ("sixth", _("Sixth")),
    ("seventh", _("Seventh")),
    ("eighth", _("Eighth")),
    ("ninth", _("Ninth")),
    ("tenth", _("Tenth")),
    ("eleven", _("Eleven")),
    ("middle", _("Middle")),
    ("basic", _("Basic")),
)

COURSES_IN_BASIC = ["sixth", "seventh", "eighth", "ninth", "basic"]
COURSES_IN_MIDDLE = ["tenth", "eleven", "middle"]

ORDERED_COURSES_FOR_UNDERAGE = ["sixth", "seventh", "eighth", "ninth", "tenth", "eleven"]

LAST_APPROVED_FOR_BASIC = ["incomplete-elementary", "complete-elementary", "sixth", "seventh", "eighth"]

LAST_APPROVED_FOR_MIDDLE = ["ninth", "tenth"]

LAST_APPROVED_GRADE_CHOICES = (
    ("incomplete-elementary", _("Incomplete Elementary")),
    ("complete-elementary", _("Complete Elementary")),
    ("sixth", _("Sixth")),
    ("seventh", _("Seventh")),
    ("eighth", _("Eighth")),
    ("ninth", _("Ninth")),
    ("tenth", _("Tenth")),
)

PAYMENT_METHOD_CHOICES = (
    ("office", _("Office")),
    ("bancolombia", _("Bancolombia")),
    ("payu", _("Payu")),
    ("efecty", _("Efecty")),
    ("western-union", _("Western Union")),
    ("paypal", _("Paypal")),
    ("other", _("Other")),
)

SEMESTER_CHOICES = (
    ("2016-01", _("2016-01")),
    ("2016-02", _("2016-02")),
    ("2017-01", _("2017-01")),
    ("2017-02", _("2017-02")),
    ("2018-01", _("2018-01")),
    ("2018-02", _("2018-02")),
    ("2019-01", _("2019-01")),
    ("2019-02", _("2019-02")),
    ("2020-01", _("2020-01")),
    ("2020-02", _("2020-02")),
)
